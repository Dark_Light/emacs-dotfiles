;;; util.el --- Utilities -*- lexical-binding: t -*-

(eval-when-compile
  (require 'cl-lib))

(cl-block :util
  (let* ((base (file-name-sans-extension (or load-file-name "")))
	 (source (concat base ".el"))
	 (byte (concat source "c")))
    (when (and load-file-name (file-newer-than-file-p source byte))
      (byte-recompile-file source nil 0)
      (load base t t)
      (cl-return-from :util)))
  
  (defmacro with-errors-as-warnings (type message &rest body)
    "Like `with-demoted-errors', except that MESSAGE can be anything that evals to a string acceptable to `format', and errors are displayed as warnings instead of messages

TYPE is the same as in `display-warning', and defaults to emacs"
    (declare (indent defun))
    (let ((signal (gensym "signal")))
      `(condition-case-unless-debug ,signal
	   (progn ,@body)
	 (error (display-warning ,(or type 'emacs) (format ,(or message "%s") ,signal) :error) ( )))))

  (defun load-maybe-recompile (source)
    "(Re)compile as necesary and silently load FILENAME.

If FILENAME has an up to date .elc counterpart don't recompile, but still load FILENAME"
    (let* (force-load-messages
	   (source (file-name-sans-extension source))
	   (filename (format "%s.el" source)))
      (byte-recompile-file filename nil 0)
      (load (file-name-sans-extension source) t t)))

  ;; naive byte compiler is naive
  (declare-function load-maybe-recompile (buffer-file-name))

  (defun reload-init-file ( )
    "Reload `user-init-file', with implicit compilation as needed, and run `after-init-hook'"
    (interactive) (load-maybe-recompile user-init-file) (run-hooks 'after-init-hook)))

(provide 'util)
