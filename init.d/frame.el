;;; frame --- config specific to all frames -*- lexical-binding: t -*-
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Config for new frames ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(declare-function init-set-theme "functions")
(declare-function monospace-font-list "functions")

(defun init-frame-config (&optional context)
  "Callback for frame specific config"
  ;; Ensure the given frame is current
  (with-selected-frame (or context (selected-frame))
    ;; Do Prefer Full Screen
    (unless (or (not (display-graphic-p))
		(memq (frame-parameter ( ) 'fullscreen) '(fullscreen fullboth)))
      (toggle-frame-fullscreen))
    (when (display-graphic-p)
      ;; Set Font
      (let ((fonts (delete-dups (monospace-font-list))))
	(cond
	 ((member "Fira Code" fonts)
	  (set-frame-font "Fira Code-16"))
	 ((member "Inconsolata" fonts)
	  (set-frame-font "Inconsolata-19"))
	 ((member "FreeMono" fonts)
	  (set-frame-font "FreeMono-19"))))
      ;; Disable tool-bar-mode
      (tool-bar-mode -1))
    ;; On daemon instances set theme
    (when (daemonp) (init-set-theme))))

;; Add to future frames
(add-hook 'after-make-frame-functions 'init-frame-config)

;; Call it after init on current initial frame
(add-hook 'after-init-hook 'init-frame-config)

;; Disable some graphical features
(cl-loop with features = (list 'scroll-bar-mode 'menu-bar-mode 'blink-cursor-mode)
	 as feature in features
	 initially (when (display-graphic-p) (nconc features (list 'tool-bar-mode)))
	 do (when (fboundp feature) (funcall feature -1)))
