;;; compile-util.el --- compiling source file -*- lexical-binding: t -*-

(cl-block :compile-util
  (let* ((base (file-name-sans-extension (or load-file-name "")))
	 (source (concat base ".el"))
	 (byte (concat source "c")))
    (when (and load-file-name (file-newer-than-file-p source byte))
      (byte-recompile-file source nil 0)
      (load base t t)
      (cl-return-from :compile-util)))
  
  (mapc #'require '(util variables)))

(provide 'compile-util)
