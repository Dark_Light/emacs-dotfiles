;;; packages --- Extra Packages -*- lexical-binding: t; -*-

(use-package bind-key) ;;; Required for :bind keyword in use-package

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Any ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; webserver
(use-package simple-httpd :load-configuration
  :functions
  (httpd-unhex httpd-send-header httpd-send-file httpd/mlp-css httpd/mlp-error httpd/mlp-error-backtrace)
  :custom
  (httpd-host (format-network-address (car (network-interface-info "enp0s25")) t)
	      "ensure host current address is used")
  (httpd-port 3264)
  (httpd-root (expand-file-name "html" user-home-directory))
  (httpd-show-backtrace-when-error t))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Interactive ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(use-package image :unless (or noninteractive
			       (not (display-graphic-p)))
  :custom (image-animate-loop t)
  (image-use-external-converter t)
  :load-configuration :builtin)

(use-package ielm :unless noninteractive :builtin :load-configuration)

(use-package eshell :unless noninteractive :builtin :load-configuration)

(use-package lisp-mode :unless noninteractive :builtin :load-configuration)

(use-package elisp-mode :unless noninteractive :builtin :load-configuration)

(use-package calc :builtin :when (eq init-system-type 'termux)
  :functions (calc-big-or-small)
  :config (run-at-time 0 nil (lambda ( ) (calc-big-or-small t))))

(use-package doom-themes :unless noninteractive
  :custom
  (doom-themes-enable-bold t)
  (doom-themes-enable-italic t)
  :config
  (run-at-time 0 ( ) (lambda ( ) (init-set-theme 'doom-outrun-electric)))
  ;; (doom-themes-visual-bell-config)
  ;; (doom-themes-org-config)
  (add-to-list 'custom-theme-load-path (file-name-directory (locate-library "doom-themes"))))

;;; control webpages from emacs
(use-package skewer-mode :unless noninteractive
  :after simple-httpd
  :config (skewer-setup))

;;; multimedia
(use-package emms :load-configuration :disabled :unless noninteractive)

;;; better parens logic
(use-package smartparens :load-configuration :unless noninteractive)

;;; better macroexpansion
(use-package macrostep :unless noninteractive
  :defines ielm-map
  :bind
  (:map
   lisp-mode-shared-map
   ("C-c e" . macrostep-expand))
  :config
  (with-eval-after-load 'ielm
    (bind-key "C-c e" 'macrostep-expand ielm-map)))

(use-package sly :demand :load-configuration :unless noninteractive
  :custom
  (sly-port 3009)
  (sly-kill-without-query-p t)
  (sly-auto-select-connection 'always)
  (sly-enable-evaluate-in-emacs t)
  (sly-net-coding-system 'utf-8-unix)
  (sly-mrepl-history-file-name (expand-file-name (file-name-concat "sly" "mrepl-history") user-emacs-directory))
  :bind
  ("C-x y" . sly)
  ("C-:" . sly-interactive-eval))

(use-package sly-macrostep :after sly macrostep :unless noninteractive)

(use-package js2-mode :disabled :unless noninteractive
  :config (add-to-list 'auto-mode-alist `(,(rx ".js" eol ) . js2-mode)))

;;; Lua
(use-package lua-mode :disabled :unless noninteractive
  :if (executable-find "lua")
  :config
  (add-to-list 'auto-mode-alist `(,(rx ".lua" eol) . lua-mode))
  (add-to-list 'interpreter-mode-alist '("lua" . lua-mode)))

;;; better git management
(use-package magit :unless noninteractive
  :if (executable-find "git")
  :bind ("C-x g" . magit-status)
  (:map magit-mode-map
	("." . magit-checkout))
  :config
  (when (file-directory-p (user-git-repository-directory))
    (add-to-list 'magit-repository-directories (cons (user-git-repository-directory) 1)))
  (let ((guix-channels-directory (expand-file-name (file-name-concat "guix" "local-channels") (xdg-config-home))))
    (when (file-directory-p guix-channels-directory)
      (add-to-list 'magit-repository-directories (cons guix-channels-directory 1))))
  (add-to-list 'magit-repository-directories (cons user-emacs-directory 0))
  (add-to-list 'magit-repository-directories (cons user-home-directory 0)))

;;; better multi terminal management
(use-package multi-term :unless noninteractive
  :bind ("<f12>" . multi-term-dedicated-toggle))

(use-package helm :unless noninteractive
  :defines eshell-mode-map
  :functions eshell-cmpl-initialize
  :hook
  (eshell-mode . (lambda ( )
		   (eshell-cmpl-initialize)
		   (define-key eshell-mode-map [remap-eshell-pcomplete] 'helm-esh-pcomplete)
		   (define-key eshell-mode-map (kbd "M-p") 'helm-eshell-history)))
  :bind
  ("M-x" . helm-M-x)
  ("C-x C-f" . helm-find-files)
  ("C-x C-b" . helm-mini)
  ("C-x b" . helm-mini)
  ("C-x p" . helm-list-emacs-process))

(use-package nov :load-configuration :unless noninteractive
  :bind
  (:map nov-mode-map
	("<up>" . nov-scroll-down)
	("<down>" . nov-scroll-up)
	("<left>" . nov-previous-document)
	("<right>" . nov-next-document))
  :mode ((rx (* anything) "." "epub") . nov-mode))
