;;; misc.el --- Miscellaneous Configurations -*- lexical-binding: t -*-
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Miscellaneous ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(cl-eval-when (:compile-toplevel)
  (add-to-list 'load-path default-directory)
  (require 'compile-util))

;; In termux, remove temporary files over a week stale
(when (eq init-system-type 'termux)
  (cl-loop with default-directory = (temporary-file-directory)
	   as entry in (directory-files default-directory ( ) (rx (not ".")))
	   as attributes = (file-attributes entry)
	   with limit = (* 3600 24 7) ;; a week
	   with now = (time-to-seconds)
	   when (> (- now (time-to-seconds (file-attribute-modification-time attributes))) limit)
	   do (if (eq (file-attribute-type attributes) t) ;; directory, or something else
		  (delete-directory entry t)
		(delete-file entry))))

(defun init-start-picolisp ( )
  "Start a picolisp process and set up an interactive buffer for the repl"
  (interactive)
  (when-let ((picolisp (executable-find "picolisp")))
    (let* ((root (cl-case init-system-type
		   (guix (file-name-parent-directory (file-name-directory (file-truename picolisp))))
		   (termux (file-name-as-directory (file-name-concat (getenv "PREFIX") "lib" "picolisp")))))
	   (lib (expand-file-name "lib.l" root))
	   (ext (expand-file-name "ext.l" root)))
      (async-shell-command (format "picolisp %s %s +" lib ext) (generate-new-buffer-name "*picolisp*")))))
