;;; server.el --- Daemon Specific Config -*- lexical-binding: t -*-

;; Because server built in functionality may be loaded long after
;; init, and we need it now, we require it now
(require' server)

;; Server PID files
(defun init-daemon-pidfile-path ( )
  "Return a filename path suitable as a pidfile for this daemon instance"
  (file-name-concat user-emacs-directory "daemon-pids" (or (daemonp) "default")))

(defun init-daemon-create-pidfile ( )
  (let ((pidfile (init-daemon-pidfile-path)))
    (unless (file-exists-p (file-name-directory pidfile))
      (mkdir (file-name-directory pidfile) t))
    (with-temp-file pidfile
      (insert (number-to-string (emacs-pid))))))

(defun init-daemon-remove-pidfile ( )
  (let ((pidfile (init-daemon-pidfile-path)))
    (when (file-exists-p pidfile) (delete-file pidfile))))

(add-hook 'after-init-hook 'init-daemon-create-pidfile)
(add-hook 'kill-emacs-hook 'init-daemon-remove-pidfile)

;; Make it so we can restart the server via signals
(defun init-daemon-restart ( )
  "Handler for SIGUSR1 signal, to (re)start an emacs server.

Can be tested from within emacs with: (signal-process (emacs-pid)
  \='sigusr1)

or from the command line with: $ kill -USR1 <emacs-pid> $ emacsclient
-c"
  (interactive)
  (server-force-delete)
  (server-start))

(define-key special-event-map [sigusr1] 'init-daemon-restart)

(defun init-daemon-self-start ( )
  "Raise an existing or newly created frame on $DISPLAY"
  (when-let* ((context (getenv "DISPLAY"))
	      (canon (if (string-match-p (rx line-start ?: digit ?. digit (zero-or-one ?. digit) line-end)  context)
			 context
		       (format "%s.0" context))))
    (with-selected-frame (or (car (ignore-errors (frames-on-display-list canon)))
			     (make-frame-on-display canon))
      (with-selected-window (frame-selected-window)
	(raise-frame)
	(switch-to-buffer (initial-buffer-choice))))))

(when (getenv "DISPLAY") (add-hook 'after-init-hook 'init-daemon-self-start))
