;;; functions --- Utility Functions -*- lexical-binding: t -*-
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; General Utilities ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(cl-eval-when (:compile-toplevel)
  (add-to-list 'load-path default-directory)
  (require 'compile-util))

(defun load-configuration (basename)
  "Load the elisp file BASENAME relative to
  `user-elisp-config-directory', taking care of auto compilation.

Any error while loading BASENAME is reported as a warning"
  (with-errors-as-warnings '(configuration) (format "(load-maybe-recompile %S): %%S" basename)
    (load-maybe-recompile (expand-file-name (format "%s.el" basename) user-elisp-config-directory))))

(defun load-directory (library &optional recurse bytecomp)
  "Load the lisp files from a directory LIBRARY, without suffixes.

Optional argument RECURSE non-nil means load files from the whole tree instead

If BYTECOMP is non nil, auto (re)compile files as necesary

When called interactively, prompt for directory, and recurse argument is `universal-argument'

Return a list of the loaded files or nil if none"
  (interactive "D\nP")
  (when (file-directory-p library)
    (let ((files (if recurse
		     (directory-files-recursively library ".*elc?$")
		   (directory-files library t ".*elc?$")))
	  loaded)
      (setf files (delete-dups (let (result)
				 (dolist (string files (nreverse result))
				   (push (replace-regexp-in-string "\.elc?$" "" string) result)))))
      (dolist (it files (nreverse loaded))
	(when bytecomp
	  (byte-recompile-file (concat it ".el") nil 0))
	(and (load it t t)
	     (push it loaded)))
      loaded)))

(let (current-theme)
  (cl-defun init-set-theme (&optional (theme current-theme))
    "Load or enable theme THEME as necesary, and disable all others, except user, as it contains the custom config

Argument THEME must be a symbol included in either `custom-available-themes' or `custom-known-themes' and defaults to the current one, or `init-theme' if either the current theme is unavailble, or an invalid value is passed

On daemon instances this function is called by `init-frame-config' to reenable the current theme

Returns the theme actually set"
    (unless (and (custom-theme-name-valid-p theme)
		 (or (memq theme (custom-available-themes))
		     (memq theme custom-known-themes)))
      (setf theme init-theme))
    (let ((previous-theme current-theme))
      (unless (eq theme current-theme)
	(setf current-theme theme))
      (condition-case ( )
	  (if (custom-theme-enabled-p current-theme)
	      (enable-theme current-theme)
	    (load-theme current-theme t))
	(error
	 (setf current-theme previous-theme)
	 (enable-theme current-theme))))
    (cl-loop as theme in custom-enabled-themes
	     with exceptions = `(,current-theme user)
	     unless (member theme exceptions)
	     do (disable-theme theme)
	     finally return current-theme)))

(defun init-enable-lexical-binding-globally ( )
  "Turn on `lexical-binding' globally"
  (set-default 'lexical-binding t))

(defun initial-buffer-choice ( )
  "Return the buffer to select at init and when connecting to a daemon instance, creating that buffer if necesary

The default is to switch to the scratch buffer in a non interactive instance, and to an IELM repl otherwise

By default, buffers that are created within the dynamic extent of a call to this function have `default-directory' set to `user-home-directory'

To override the default add a function to `initial-buffer-choice-functions'"
  (save-excursion
    (save-window-excursion
      (cl-loop as function in initial-buffer-choice-functions
	       as buffer = (with-errors-as-warnings 'initial-buffer-choice
			     (format "%s: %%s" (or (and (symbolp function)
							(symbol-name function))
						   "Anonymous Lambda"))
			     (when-let ((buffer (funcall function)))
			       (and (or (bufferp buffer)
					(stringp buffer))
				    (get-buffer buffer))))
	       with default-directory = user-home-directory
	       thereis (and (buffer-live-p buffer) buffer)
	       finally return (if noninteractive
				  (with-current-buffer (get-buffer-create "*scratch*")
				    (unless (derived-mode-p 'lisp-interaction-mode)
				      (lisp-interaction-mode))
				    (current-buffer)) 
				(or (ielm "*repl*") (current-buffer)))))))

(cl-defun file-to-list (filename &optional (regexp (rx (+ whitespace))))
  "Return the contens of FILENAME as a list, where each element is delimited by REGEXP

Empty strings and duplicates are removed from the result"
  (when (and (stringp filename)
	     (file-regular-p filename))
    (with-temp-buffer
      (insert-file-contents-literally filename)
      (delete-dups (split-string (buffer-string) regexp t)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Miscellaneous ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun init-exwm ( )
  "Load and enable EXWM. This function should only be called on an emacs instance meant to be used as a window manager"
  (unless (or (featurep 'exwm)
	      (member system-type '(ms-dos windows-nt cygwin)))
    (load-configuration "exwm")))

(defun monospace-font-list ( )
  "Return a list of monospaced fonts"
  (cl-loop as font in (x-family-fonts)
	   as name = (aref font 0)
	   as monospace = (aref font 5)
	   when (and name monospace)
	   collecting (symbol-name name) into fonts
	   finally return (delete-dups fonts)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Batch ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(when noninteractive
  (defun batch-inhibit-automatic-exit ( )
    "Disable automatic exit of this batch instance by advising `kill-emacs' to be a noop when called from `command-line', after which the advise self removes. This effectively leaves emacs runnning after init and command line processing"
    (eval-and-compile (require 'backtrace))
    (advice-add 'kill-emacs :before-until
		(lambda (&optional _arg _restart)
		  (let ((frames (backtrace-get-frames 'kill-emacs)))
		    (when (and (member 'command-line (mapcar #'backtrace-frame-fun frames))
			       (equal '(t) (backtrace-frame-args (cl-first frames))))
		      (advice-remove 'kill-emacs 'maybe-noop)
		      t)))
		'((name . maybe-noop)))))
