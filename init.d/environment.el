;;; environment --- Miscellaneous Config -*- lexical-binding: t -*-

(cl-eval-when (:compile-toplevel)
  (add-to-list 'load-path default-directory)
  (require 'compile-util)
  (load-maybe-recompile "functions"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Basic settings ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(setf print-circle t
      print-gensym t
      backup-inhibited t
      disabled-command-function nil
      integer-width 62914561
      visible-bell t
      current-time-list nil)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Coding System ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(prefer-coding-system 'utf-8)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Aliases ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(pcase-dolist (`(,symbol ,function) '((loop cl-loop) (repl ielm) (y-or-n-p yes-or-no-p)))
  (when (not (fboundp symbol)) (defalias symbol function)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Ensure certain environment variables are present ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(let* ((y (format "%s --alternate-editor=\"\" --socket-name=%s"
		  (expand-file-name "emacsclient" (file-name-directory (car command-line-args))) (getenv "USER")))
       (variables `(("XDG_CONFIG_HOME" . ,(expand-file-name ".config" user-home-directory))
		    ("XDG_DATA_HOME" . ,(expand-file-name (file-name-concat ".local" "share") user-home-directory))
		    ("XDG_CACHE_HOME" . ,(expand-file-name ".cache" user-home-directory))
		    ("EDITOR" . ,y)
		    ("VISUAL" . ,y)
		    ("PAGER" . "cat")
		    ;; ("GSETTINGS_SCHEMA_DIR" . "/usr/share/glib-2.0/schemas")
		    )))
  ;; I don't know why, but the alias is still not effective at this time
  (cl-loop as (key . value) in variables 
	   when (null (getenv key))
	   do (setenv key value)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; path ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(add-to-list 'load-path user-lisp-directory)

(require 'xdg)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Git Environment ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define-inline user-git-repository-directory ( )
  "Return the location for git repositories, relative to `xdg-config-home'"
  (inline-quote (expand-file-name (file-name-concat "git" (file-name-as-directory "repositories"))
				  (xdg-config-home))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Guix Environment ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(cl-defun init-guix-extra-profiles (&optional (profiles-directory
					       (or (getenv "GUIX_EXTRA_PROFILES")
						   (expand-file-name (file-name-concat "guix" "profiles")
								     (xdg-config-home)))))
  "Return the list of guix extra profiles"
  (when init-guix-available-p
    (when (file-directory-p profiles-directory)
      (cl-loop as manifest in (directory-files profiles-directory t (rx (and (* anything) ?. "scm")))
	       as base = (file-name-sans-extension manifest)
	       collecting (format (file-name-concat "%s" "%s") base (file-name-base base))))))
  
(defun init-guix-packages-profile ( )
  "Return the profile containing emacs packages"
  (when init-guix-available-p
    (cl-loop as profile in (init-guix-extra-profiles)
	     thereis (and (string-match-p (rx (and anything "emacs-packages" anything)) profile) profile))))

;; On Guix, but not Guix System, add info entries from guix profiles

(when (and init-guix-available-p
	   (not (eq init-system-type 'guix)))
  (cl-loop as profile in `(,(getenv "GUIX_PROFILE") ,(init-guix-packages-profile))
	   as context = (file-name-as-directory (expand-file-name (file-name-concat "share" "info") profile))
	   initially (eval-and-compile (require 'info))
	   do (when (file-directory-p context)
		(add-to-list 'Info-directory-list context))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Initial Buffer Choice ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(setf initial-buffer-choice 'initial-buffer-choice)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Hooks ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Coding Mode Hooks
(cl-flet ((coding-mode (lambda ( ) (variable-pitch-mode t) (hl-line-mode))))
  (run-at-time 0 nil
	       (lambda ( )
		 (dolist (code-mod init-coding-modes-hooks)
		   (add-hook code-mod #'coding-mode)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Functions Modifications ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defvar temporary-buffers nil
  "List of buffers noted to be created with `inhibit-buffer-hooks' non-nil")

(advice-add 'save-buffers-kill-emacs :before
	    (lambda (&rest args) "Run `before-exit-hook'" (ignore args) (run-hooks 'before-exit-hook))
	    '((name . before-kill-emacs)))

(advice-add 'get-buffer-create :after
	    (lambda (buffer-or-name &optional inhibit-buffer-hooks)
	      (when (and inhibit-buffer-hooks
			 (not (bufferp buffer-or-name)))
		(setf (alist-get (get-buffer buffer-or-name) temporary-buffers) inhibit-buffer-hooks)))
	    '((name . log-temp-buffer)))

(advice-add 'kill-buffer :around
	    (lambda (kill-buffer &optional buffer)
	      "Run `after-kill-buffer-functions' if BUFFER is actually killed."
	      ;; Coerce buffer into an actual buffer to avoid ambiguity
	      (unless (bufferp buffer)
		(setf buffer
		      (cl-typecase buffer
			(string (get-buffer buffer))
			(null (current-buffer))
			(otherwise (signal 'wrong-type-argument `((or bufferp stringp null) ,buffer))))))
	      (if (alist-get buffer temporary-buffers)
		  (prog1 (funcall kill-buffer buffer)
		    (setf (alist-get buffer temporary-buffers ( ) t) ( )))
		(let* ((name (buffer-name buffer))
		       ;; If there is a buffer local value for the hook, we want to run it alongside
		       ;; the global value, which means preserving it after the actual buffer is gone
		       (local (local-variable-p 'after-kill-buffer-functions buffer))
		       (context (and local (buffer-local-value 'after-kill-buffer-functions buffer)))
		       (killedp (funcall kill-buffer buffer)))
		  ;; Only run the hook when the buffer is actually killed
		  ;; and is not a temporary buffer
		  (when killedp
		    ;; If there is a local value, do the following:
		    ;; if the current buffer has its own local value, temporarily overwrite it
		    ;; else make a temporal buffer local variable and set it to the saved value
		    (let ((ext-local (local-variable-p 'after-kill-buffer-functions))
			  (current-buffer (current-buffer)))
		      (unless (or (not local) ext-local)
			(make-local-variable 'after-kill-buffer-functions))
		      (unwind-protect
			  ;; Preserve current buffer
			  (save-current-buffer
			    ;; We call the hook with both the name and the actual object
			    ;; in the case that some handler has a reference to the buffer to compare
			    (if local
				(let ((after-kill-buffer-functions context))
				  (run-hook-with-args 'after-kill-buffer-functions name buffer))
			      (run-hook-with-args 'after-kill-buffer-functions name buffer)))
			(unless (or (not local) ext-local
				    (not (buffer-live-p current-buffer)))
			  (kill-local-variable 'after-kill-buffer-functions)))))
		  killedp)))
	    '((name . after-kill-buffer)))

(when noninteractive
  (advice-add 'load :around (lambda (load &rest args)
			      "Ensure `load' always loads a file silently"
			      (cl-destructuring-bind (file &optional noerror nomessage &rest args) args
				(setf nomessage t)
				(let (force-load-messages) ;; I mean it!
				  (apply load file noerror nomessage args))))))

;; Ensure lexical-binding is default everywhere
;; Also run some other things after init
(run-at-time 0 nil 'init-enable-lexical-binding-globally)
