;;; straight-packages --- Extra Packages Via Straight -*- lexical-binding: t; no-byte-compile: t; -*-

(cl-eval-when (:compile-toplevel)
  (load (expand-file-name "package-configuration")))

(use-package bind-key) ;;; Required for :bind keyword in use-package

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Installed via straight ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; picolisp support
(use-package plisp-mode :load-configuration
  :if (executable-find "picolisp")
  :straight t)

(unless (daemonp)
  (use-package rebecca-theme
    :straight t
    :config (init-set-theme 'rebecca)))

;;; Themes
(use-package laguna-theme :disabled :straight t)

;;; common lisp implementation
(use-package emacs-cl :disabled
  :straight (let ((root (expand-file-name (file-name-concat "source" "emacs-cl") user-home-directory)))
	      (list :type 'git
		    :local-repo root
		    :files (cl-loop with src = "src"
				    as entry in (directory-files (expand-file-name src root) nil)
				    when (string= "el" (file-name-extension entry)) collect (concat src entry)))))

(use-package aweshell :disabled
  :straight (:type git :host github :repo "manateelazycat/aweshell")
  :config (setf eshell-prompt-function 'epe-theme-dakrone))
