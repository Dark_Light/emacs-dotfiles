;;;  bindings --- Custom Bindings Config -*- lexical-binding: t -*-
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Global Bindings ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(pcase-dolist (`(,key . ,exp) `((,(kbd "<f5>") . revert-buffer) (,(kbd "<XF86AudioMedia>") . emms)
				(,(kbd "<XF86Search>") . xwidget-webkit-browse-url) (,(kbd "C-h M-f") . find-function)
				(,(kbd "C-h M-v") . find-variable) (,(kbd "C-h M-l") . find-library)
				(,(kbd "C-h M-k") . find-function-on-key) (,(kbd "<XF86MyComputer>") . customize)
				(,(kbd "M-#") . eshell-command) (,(kbd "C-c l") . org-store-link)
				(,(kbd "C-c a") . org-agenda) (,(kbd "C-c c") . org-capture)
				(,(kbd "C-x C-k") . kill-current-buffer)))
  (global-set-key key exp))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Isearch ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; set arrow keys in isearch. left/right is backward/forward, up/down is history. press Return to exit
(pcase-dolist (`(,key . ,exp) `((,(kbd "<up>") . isearch-ring-retreat) (,(kbd "<down>") . isearch-ring-advance)
				(,(kbd "<left>") . isearch-repeat-backward)
				(,(kbd "<right>") . isearch-repeat-forward)))
  (define-key isearch-mode-map key exp))

(pcase-dolist (`(,key . ,exp) `((,(kbd "<left>") . isearch-reverse-exit-minibuffer)
				(,(kbd "<right>") . isearch-forward-exit-minibuffer)))
  (define-key minibuffer-local-isearch-map key exp))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; xwidgets ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(with-eval-after-load 'xwidget
  '(pcase-dolist (`(,key . ,exp) `((,(kbd "<prior>") . xwidget-webkit-scroll-page-up)
				   (,(kbd "<next>") . xwidget-webkit-scroll-page-down)
				   (,(kbd "SPC") . xwidget-webkit-scroll-page-down)
				   (,(kbd "<home>") . xwidget-webkit-scroll-top)
				   (,(kbd "<end>") . xwidget-webkit-scroll-bottom)
				   (,(kbd "f") . xwidget-webkit-forward)))
     (define-key xwidget-webkit-mode-map key exp)))

(eval-and-compile
  (require 'dired))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Dired ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define-key dired-mode-map [S-return] 'sudo-edit-current-file)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Miscellaneous ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; in termux, bind <f11> (fingerprint sensor) to eval-expression
(when (and (eq init-system-type 'termux)
	   (not (display-graphic-p)))
  (global-set-key (kbd "<f11>") 'eval-expression))
