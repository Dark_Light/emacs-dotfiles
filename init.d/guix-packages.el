;;; guix-packages --- Guix Specific Packages  -*- lexical-binding: t; no-byte-compile: t; -*-

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Installed via guix ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Any ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; Buffer Local Environments
(use-package buffer-env
  :custom
  (buffer-env-script-name (list "manifest.scm" "guix.scm"))
  (buffer-env-mode-line '(:eval (format " Guix-Env[%s]" (car-safe (last (split-string default-directory "/" t))))))
  :config
  (add-hook 'hack-local-variables-hook 'buffer-env-update)
  (add-hook 'comint-mode-hook 'hack-dir-local-variables-non-file-buffer)
  (add-hook 'dired-mode-hook 'hack-dir-local-variables-non-file-buffer))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Interactive ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; Telegram Client
(use-package telega :unless noninteractive
  :if (fboundp 'telega)
  :bind ("<XF86Mail>" . telega))

;;; Git Annex Integration
(use-package git-annex :unless noninteractive)
