;;; variables.el --- Miscellaneous Variable Declarations -*- lexical-binding: t -*-

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Basic Variables ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(require 'xdg)

(defconst user-home-directory (let ((home (getenv "HOME"))) (and home (file-name-as-directory home)))
  "The home directory of the user, taken from the HOME environment variable")

(defvar user-downloads-directory (file-name-as-directory (or (xdg-user-dir "DOWNLOAD")
							     (expand-file-name "Descargas" user-home-directory)))
  "Directory upon which downloaded files are placed")

(defvar user-media-directory (expand-file-name (file-name-as-directory "Media") user-home-directory)
  "Location of media files. This includes books, images, and game roms")

(defvar user-notes-directory (expand-file-name (file-name-as-directory "Notas") user-media-directory)
  "Directory for notes of any kind")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; My personal lisp library ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defconst user-init-directory (let ((library (expand-file-name (file-name-as-directory "init.d")
							       user-emacs-directory)))
				(and (file-directory-p library)
				     library))
  "Location to look for additional initialization files.

Always relative to `user-emacs-directory'")

(defconst user-lisp-directory (expand-file-name (file-name-as-directory "lisp") user-emacs-directory)
  "Location on which personal proyects are placed.

Like `user-init-directory', it is always relative to `user-emacs-directory'")

(defvar user-elisp-config-directory (expand-file-name (file-name-as-directory "configuration") user-emacs-directory)
  "Directory in which to place misc config elisp files beyond those of `user-init-directory'

Normally, such files contains the configuration of a package, and thus are often loaded from a `use-package' form.
To load them, use `load-configuration'")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Platforms ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defvar init-guix-available-p
  (and (executable-find "guix") t)
  "Boolean that tells whether guix is available")

(defconst init-system-type
  (if (eq system-type 'gnu/linux)
      (or (and init-guix-available-p
	       (file-exists-p
		(file-truename
		 (file-name-concat "/" "run" "current-system" "configuration.scm")))
	       'guix)
	  (and (string-match-p (rx line-start "/data" (*? anything) "com.termux" (*? anything))
			       user-home-directory)
	       (cl-loop as entry in process-environment
			thereis (integerp (string-match-p (rx line-start "TERMUX" (* anything)) entry)))
	       'termux)
	  system-type)
    system-type)
  "Like `system-type', but with more platforms, such as \"guix\" and \"termux\"")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Theme ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defvar init-theme 'adwaita
  "The default theme, read by `init-set-theme' as a fallback.

In daemon instances, at frame creation time, and after init,
`init-frame-config' calls `init-set-theme' without arguments,
which either reenables the last specified theme, or loads the one
from this variable

To actually set a theme, call `init-set-theme' with the desired
theme as argument

Value should always be a symbol present in the list returned by
`custom-available-themes'")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Initial Buffer Choice ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defvar initial-buffer-choice-functions ( )
  "List of functions that are called with no arguments and should
return a buffer or nil. If they error out, that is interpreted as
nil

The first function to return a buffer has said buffer chosen, so
they should be ordered by preference")


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Hooks ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defvar before-exit-hook ( ) "Hook to run early by `save-buffers-kill-emacs'")

(defvar after-kill-buffer-functions ( )
  "Hook run by `kill-buffer' after actually killing a buffer.
 The functions are run with two arguments, the name of the killed
buffer, and the buffer object

Unlike `kill-buffer-hook', functions added here are guaranteed
that the buffer has been killed.  This is useful for post
cleanups and buffer sentinels, see `eshell-regenerate-session'
for an example

Also unlike `kill-buffer-hook', functions added here may run with
any buffer current, and are not required to keep the current
buffer as such")

(defvar init-coding-modes-hooks '(html-mode-hook js-mode-hook term-mode-hook sh-mode-hook c-mode-common-hook scheme-mode-hook minibuffer-setup-hook)
  "List of hooks from modes which can be classed as programming modes. Several common functions are added to them at init")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Mail ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defvar user-gnus-directory (expand-file-name (file-name-as-directory "gnus") user-emacs-directory)
  "Location of gnus specific config")

(setf gnus-init-file (expand-file-name "gnus-init.el" user-gnus-directory))

(provide 'variables)
