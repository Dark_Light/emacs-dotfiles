;;; package-configuration --- Package Configuration -*- lexical-binding: t; -*-

(eval-when-compile
  (require 'cl-lib))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Guix ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(cl-eval-when (:compile-toplevel)
  (add-to-list 'load-path default-directory)
  (require 'compile-util))

(declare-function init-guix-packages-profile "environment")

(when init-guix-available-p
  (when (file-directory-p (init-guix-packages-profile))
    (load (expand-file-name (file-name-concat "share" "emacs" "site-lisp" "subdirs.el")
			    (init-guix-packages-profile))))

  (declare-function guix-emacs-autoload-packages nil)
  
  ;; Run guix-emacs system
  (require 'guix-emacs)
  (guix-emacs-autoload-packages))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Straight ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; compiler
(defvar straight-check-for-modifications)
(defvar straight-use-package-by-default)
(declare-function load-configuration "functions")

;;; Straight boostrap
(defvar init-straight-available-p
  (unless init-guix-available-p
    (setf straight-check-for-modifications '(find-when-checking)
	  straight-use-package-by-default t)
    (let ((bootstrap-file
	   (expand-file-name (file-name-concat "straight" "repos" "straight.el" "bootstrap.el")
			     user-emacs-directory)))
      (unless (file-exists-p bootstrap-file)
	(with-current-buffer
	    (url-retrieve-synchronously
	     "https://raw.githubusercontent.com/radian-software/straight.el/develop/install.el"
	     'silent 'inhibit-cookies)
	  (goto-char (point-max))
	  (eval-print-last-sexp)))
      (load bootstrap-file nil 'nomessage))
    (load-configuration "straight"))
  "Boolean that tells wether `straight.el' is available in this session")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Use Package ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(declare-function straight-use-package nil)
(unless init-guix-available-p (straight-use-package 'use-package)) ;;; enable use-package
(require 'use-package) ;;; load use-package

(defvar use-package-expand-minimally)
(setf use-package-expand-minimally t) ;;; minimal fuss

(defvar use-package-keywords)
(declare-function use-package-concat nil)
(declare-function use-package-process-keywords nil)
(declare-function use-package-handler/:straight nil)

;; Add custom keyword to use-package
;; for loading configuration files
(unless (memq :load-configuration use-package-keywords)
  (push :load-configuration (nthcdr (1+ (cl-position :config use-package-keywords)) use-package-keywords)))

(defun use-package-normalize/:load-configuration (symbol _ args)
  (cl-loop as entry in args
	   as name = (cl-typecase entry
		       (boolean ( ))
		       (symbol (symbol-name entry))
		       (string entry)
		       (list (eval entry t)))
	   when name collecting name into names
	   finally return (or (delete-dups names) (list (symbol-name symbol)))))

(defun use-package-handler/:load-configuration (symbol _ entries rest state)
  (cl-loop as entry in entries collecting `(load-configuration ,entry) into entries
	   finally return (use-package-concat (use-package-process-keywords symbol rest state)
					      entries)))


;; Add :builtin keyword for packages that are builtin and should not
;; be fetched by external sources. This is specifically meant to
;; bypass keywords such as :straight and :ensure if present implicitly
;; or explicitly (see straight-use-package-by-default for an
;; example). The requirements for this to work are that this keyword
;; has higher precedence than the keywords to inhibit, and that the
;; keywords in question are known
(unless (memq :builtin use-package-keywords)
  (push :builtin use-package-keywords))

(defun use-package-normalize/:builtin (&rest _) t)

(defun use-package-handler/:builtin (symbol _keyword _args rest state)
  (setf rest (cl-loop with exceptions = (list :straight :ensure)
		      as element in rest by (lambda (current)
					      (if (member (car current) exceptions)
						  (let ((rest (cdr current)))
						    (while (and (consp rest)
								(not (keywordp (car rest))))
						      (setf rest (cdr rest)))
						    rest)
						(cdr current)))
		      unless (member element exceptions) collecting element))
  (use-package-process-keywords symbol rest state))

(cl-flet ((handler (lambda (handler name keyword args rest state)
		     "Inhibit this keyword's handler when `:builtin'
keyword is present in the `use-package' form"
		     (if (plist-get rest :builtin)
			 (use-package-process-keywords name rest state)
		       (funcall handler name keyword args rest state)))))
  (advice-add 'use-package-handler/:ensure :around #'handler '((name . maybe-noop)))
  (when init-straight-available-p
    (advice-add 'use-package-handler/:straight :around #'handler '((name . maybe-noop)))))

;; (while (not (keywordp (car entry))) (pop entry))

;; (symbol-function 'use-package-handler/:ensure)

(load-maybe-recompile (expand-file-name "base-packages" user-init-directory))
(load-maybe-recompile (expand-file-name "packages" user-init-directory))
(when init-straight-available-p (load (expand-file-name "straight-packages" user-init-directory)))
(when init-guix-available-p (load (expand-file-name "guix-packages" user-init-directory)))
