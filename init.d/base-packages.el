;;; base-packages --- Base Packages  -*- lexical-binding: t; -*-

;; Packages here are always present and expected to be usefull regardless
;; of the platform (Unix - Gnu/Linux, Windows, MacOS and others) and type
;; or kind of this instance (CLI, GUI, Standalone, Noninteractive and
;; Daemon)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Built In ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(use-package tramp :builtin :load-configuration)

(use-package auth-source-pass :builtin :when (when-let ((entry (getenv "PASSWORD_STORE_DIR")))
					       (file-accessible-directory-p entry))
  :custom (auth-sources '(password-store))
  :config (auth-source-pass-enable))

(use-package recentf :builtin :custom (recentf-max-saved-items ( )))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Packages ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(use-package aio)

(use-package stream)

(use-package pcre2el)

;;; (init-add-local-packages 'mlp 'nelly)
