;; Base settings
(setf gnus-check-new-newsgroups nil
      gnus-save-newsrc-file nil
      gnus-read-newsrc-file nil
      gnus-startup-file (expand-file-name "news" user-gnus-directory))

;; main mail entry
;; gmail
(setf gnus-select-method
      '(nnimap "gmail"
	       (nnimap-address "imap.gmail.com")  ; it could also be imap.googlemail.com if that's your server.
	       (nnimap-server-port "imaps")
	       (nnimap-stream ssl)
	       (nnmail-expiry-target "nnimap+gmail:[Gmail]/Trash") ; Move expired messages to Gmail's trash.
	       (nnmail-expiry-wait immediate))) ; Mails marked as expired can be processed immediately.

;; read system mail
(setf mail-sources `((system . (file :path ,(getenv "MAIL")))))
