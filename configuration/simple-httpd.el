;;; -*- lexical-binding: t -*-

(eval-when-compile
  (require 'simple-httpd))

(require 'server)

(defvar httpd/mlp-html
  '((400 . "<!DOCTYPE html>
<html>
<head>
<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\" />
<style>%s</style>
<title>400 Bad Request</title>
</head>
<body>
<h1>400 Bad Request</h1>
<h2>Received query was faulty</h2>
<p>Error of type <span class=\"highlight\">%s</span> found: <pre>%s</pre></p>
<div id=\"backtrace\" style=\"display: %s;\">
<h2>Backtrace:</h2>
<code>%s</code>
</body>
</body>
</html>")
    (404 . "<!DOCTYPE html>
<html>
<head>
<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\" />
<style>%s</style>
<title>404 Story Not Found</title>
</head>
<body>
<h1>404 Story Not Found</h1>
<h2>The requested story couldn't be found.</h2>
<p>Error of type <span class=\"highlight\">%s</span> found: <pre>%s</pre></p>
<div id=\"backtrace\" style=\"display: %s;\">
<h2>Backtrace:</h2>
<code>%s</code>
</div>
</body>
</html>")
    (500 . "<!DOCTYPE html>
<html>
<head>
<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\" />
<style>%s</style>
<title>500 Internal Error</title>
</head>
<body>
<h1>500 Internal Error</h1>
<h2>There was a problem while procesing the request.</h2>
<p>Error of type <span class=\"highlight\">%s</span> found: <pre>%s</pre></p>
<div id=\"backtrace\" style=\"display: %s;\">
<h2>Backtrace:</h2>
<code>%s</code>
</div>
</body>
</html>"))
  "html templates for `httpd/mlp' related functions")

(defvar httpd/mlp--css
  '((":root" . ((:--blue-violet . "rgb(124, 77, 255)")
		(:--spring-green . "rgb(0, 255, 127)")
		(:--soft-black . "rgb(26, 26, 26)")
		(:--light-gray . "rgb(141, 141, 141)")
		(:--text-color . "var(--light-gray)")
		(:--bg-color . "var(--soft-black)")
		(:font . "62.5% Roboto, Inconsolata, sans-serif")
		(:background . "var(--bg-color) no-repeat center center / cover fixed")))
    (":root > body" . ((:--main-highlight-color . "var(--blue-violet)")
		       (:--secondary-highlight-color . "var(--spring-green)")
		       (:max-width . "100%")
		       (:margin . "0%")
		       (:padding . "1.5em")
		       (:text-align . "center")
		       (:line-height . "2")
		       (:word-wrap . "break-word")
		       (:font-size . "2rem")
		       (:color . "var(--text-color)")))
    ("p" . ((:margin . "0%")
	    (:padding . "0em")))
    ("h1, h2" . ((:line-height . "1.2")
		 (:text-align . "center")
		 (:background . "transparent")))
    ("h1" . ((:color . "var(--main-highlight-color)")
	     (:text-shadow . "0em 0em 0.2em currentColor")))
    ("h2" . ((:color . "var(--secondary-highlight-color)")
	     (:text-shadow . "0em 0em 0.1em currentColor")))
    ("pre" . ( ))
    (".highlight" . ((:color . "var(--main-highlight-color)")
		     (:text-shadow . "0em 0em 0.1em currentColor"))))
  "CSS styles for `httpd/mlp' related functions")

(cl-macrolet ((with-output-buffer-to-string (&rest body)
					    `(with-temp-buffer
					       (let ((standard-output (current-buffer)))
						 ,@body
						 (buffer-string))))
	      (generate-content (object)
				`(with-temp-buffer
				   (let ((standard-output (current-buffer)))
				     (princ ,object)
				     (unless (eq (char-before) 10) (insert "\n"))
				     (httpd-escape-html-buffer)
				     (buffer-string)))))
  (defun httpd/mlp-css ( )
    "Function that generates css styles for `httpd/mlp-html', used by `httpd/mlp' related functions"
    (with-output-buffer-to-string
     (cl-loop with content
	      for (selector . styles) in httpd/mlp--css do
	      (setf content (cl-loop for (property . value) in styles with content = "" do
				     (setf content (concat content (format "\n%s: %s;" (httpd--stringify property) value)))
				     finally return content))
	      when (and styles content) do
	      (insert (format "\n%s {%s\n}\n" selector content)))))

  (cl-defun httpd/mlp-error-backtrace (&key (entry-regexp "^[\t\r\n\s]*httpd/mlp-error(.*)"))
    "Function that obtains a backtrace up to the call of `httpd/mlp-error', so it's mean to be called from that function

Key argument ENTRY-REGEXP specifies an alternative regexp to use instead of the one that looks for `httpd/mlp-error'

Calls posterior to that of the line marked by ENTRY-REGEXP are discarded. Usually, the first call on backtrace is `httpd--filter'

Returns the backtrace as a string, formated for the 500 error template page on `httpd/mlp-html'"
    (with-output-buffer-to-string
     (backtrace)
     (goto-char (point-min))
     (let ((start (point)))
       (save-match-data
	 (while (not (or (eobp)
			 (looking-at entry-regexp)))
	   (move-beginning-of-line 2)))
					;(move-end-of-line 1)
       (delete-region start (point)))
     (goto-char (point-max))
     (cl-loop with count = 0
	      while (scan-sexps (point) -1) do
	      (end-of-line -0)
	      (cl-incf count)
	      (save-excursion
		(insert "</p>"))		
	      (backward-sexp)
	      (when (not (looking-back "^[\t\r\n\s]*" nil))
		(backward-sexp))
	      (save-excursion
		(insert (format "<p><span class=\"highlight\">%d</span> " count))))))

  (cl-defun httpd/mlp-error (&optional (proc nil procp) (status 404) (type 'story-not-found) (info "Story not found"))
    "Optimized variant of `httpd-error' for `httpd/mlp'. All arguments are optional

PROC may be a process object or t, any other value is aliased to `httpd-current-proc'

STATUS must be an integer and defaults to 404. Consult `httpd-status-codes' to see the posibilities

TYPE should be an error symbol and defaults to story-not-found

INFO must be a string, and defaults to \"Story not found\"

Templates for the errors comes from `httpd/mlp-html'"
    (let* ((proc* (httpd-discard-buffer))
	   (proc (or (and procp (or (processp proc) (eq proc t)) proc) proc*)))
      (httpd-log `(error ,status ,info))
      (with-temp-buffer
	(let* ((html (or (cdr (assq status httpd/mlp-html)) ""))
	       (details (concat "mlp: " (generate-content info)))
	       (backtrace (or (and httpd-show-backtrace-when-error
				   (= status 500)
				   (httpd/mlp-error-backtrace))
			      ""))
	       (show-backtrace (or (and (not (string= backtrace "")) "grid") "none")))
	  (insert (format html (httpd/mlp-css) type details show-backtrace backtrace)))
	(httpd-send-header proc :text/html status :error-type (symbol-name type))))))

(defvar user-media-directory)
(declare-function rxt-pcre-to-elisp "pcre2el")

;; Handle "mlp/" uris
(defservlet* mlp/:book :text/html ((regexp ".*" regexp-p))
  (condition-case condition
      (progn
	(let (state (root (file-truename (expand-file-name "Libros/MLP/" user-media-directory))))
	  (cond
	   (regexp-p ;; "regexp" query parameter supersedes "mlp/<somename>" path
	    (condition-case condition
		(let ((route (cl-first (directory-files root t (rxt-pcre-to-elisp regexp)))))
		  (if (and route (file-regular-p route))
		      (setf state `(:story ,route))
		    (setf state `(:story-not-found ,regexp))))
	      (rxt-invalid-regexp
	       (pcase-let ((`(,type . ,info) condition))
		 (setf state `(:error (,type . ,info)))))))
	   (book ;; unlike the query parameter, the path doesn't support regexes and so must be exact match
	    (when (not (string-match-p ".*html" book))
	      (setf book (concat book ".html")))
	    (let ((route (concat root book)))
	      (if (and route (file-regular-p route))
		  (setf state `(:story ,route))
		(setf state `(:story-not-found ,book)))))
	   (t ;; client must supply at least the query parameter or path
	    (setf state `(:error (invalid-query . "Invalid query")))))
	  (cl-case (car state)
	    (:story
	     (pcase-let ((`(,_ ,story) state)) (httpd-send-file t story)))
	    (:error
	     (pcase-let ((`(,_ (,type . ,info)) state))
	       (httpd/mlp-error t 400 (make-symbol (symbol-name type)) info)))
	    (:story-not-found
	     (pcase-let ((`(,type ,query) state))
	       (httpd/mlp-error t 404 (make-symbol (substring (symbol-name type) 1)) (format "No story matching query ( %s ) found" query)))))))
    (t
     (pcase-let ((`(,type . ,info) condition))
       (httpd/mlp-error t 500 type info)))))

(defvar user-git-repository-directory)

(defservlet tim :text/html (path)
  (setf path (let ((route (string-join (remove "tim" (split-string path "/" t)) "/")))
	       (cond ((string= route "") "index.html") (t route))))
  (let ((route (expand-file-name path (expand-file-name "TIMbrowser/" user-git-repository-directory))))
    (if (file-exists-p route)
	(httpd-send-file t route)
      (httpd-error t 404 "Cannot find the requested game file"))))

;; Don't ask confirmation for stoping web server on exit
(defun init-httpd-noquery-exit ( )
  "Arrange that exiting the daemon doesn't prompt confirmation for httpd"
  (set-process-query-on-exit-flag (get-process "httpd") ( )))

(add-hook 'httpd-start-hook 'init-httpd-noquery-exit)

;; Arrange for there being a web daemon whose cycle is the same as the main daemon instace
(when (and (daemonp) (string= server-name (user-real-login-name)))
  (add-hook 'after-init-hook 'httpd-start)
  (add-hook 'kill-emacs-hook 'httpd-stop))
