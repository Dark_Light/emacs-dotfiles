;;; -*- lexical-binding: t -*-

(eval-when-compile
  (require 'compile-util))

(add-to-list 'init-coding-modes-hooks 'emacs-lisp-mode-hook)

