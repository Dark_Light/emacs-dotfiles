;;; -*- lexical-binding: t -*-
;; EXWM
(use-package exwm
  :defines (exwm-workspace-number
	    exwm-instance-name
	    exwm-class-name
	    exwm-title
	    exwm-workspace-current-index
	    exwm-mode-map
	    exwm-input-simulation-keys
	    exwm-input-global-keys)
  :functions (exwm-config-ido
	      exwm-workspace-rename-buffer
	      exwm-workspace--count
	      exwm-workspace-switch
	      exwm-input-send-next-key
	      exwm-enable)
  :init
  ;; Minimize fringes
  (fringe-mode 1)
  :config
  (require 'exwm-config)
  (exwm-config-ido)
  ;; Initial workspace amount
  (setf exwm-workspace-number 1)
  ;; The exwm buffers naming logic
  (add-hook 'exwm-update-class-hook
	    (lambda ( )
	      (unless (or (string-prefix-p'"sun-awt-x11" exwm-instance-name)
			  (string= "gimp" exwm-instance-name))
		(exwm-workspace-rename-buffer exwm-class-name))))
  (add-hook 'exwm-update-title-hook
	    (lambda ( )
	      (when (or (not exwm-instance-name)
			(string-prefix-p'"sun-awt-x11" exwm-instance-name)
			(string= "gimp" exwm-instance-name))
		(exwm-workspace-rename-buffer exwm-title))))
  ;; Custom Functions
  ;; Switch to previous worskpace
  (defun exwm-workspace-switch-previous ( )
    "Switch to previous exwm worskpace, wrapping around to the last workspace as necesary"
    (interactive)
    (let ((min 0)
	  (max (1- (exwm-workspace--count)))
	  (current exwm-workspace-current-index))
      (exwm-workspace-switch (if (= current min) max (1- current)))))
  ;; Switch to next worskpace
  (defun exwm-workspace-switch-next ( )
    "Switch to next exwm worskpace, wrapping around to the first workspace as necesary"
    (interactive)
    (let ((min 0)
	  (max (1- (exwm-workspace--count)))
	  (current exwm-workspace-current-index))
      (exwm-workspace-switch (if (= current max) min (1+ current)))))
  (defun exwm-run-shell-command (command)
    "Run COMMAND as a shell command in a child process asynchronously, discarding output, if any"
    (interactive (list (read-shell-command "★ ")))
    (start-process-shell-command command nil command))
  ;; Global keybindings
  (setf exwm-input-global-keys
	`(;; Bind "s-r" to exit char-mode and fullscreen mode
	  ([?\s-r] . exwm-reset)
	  ;; Bind "s-w" to switch-workspace-interactively
	  ([?\s-w] . exwm-workspace-switch)
	  ;; Bind "s-x left" to previous-buffer
	  ;([?\s-x left] . previous-buffer)
	  ;; Bind "s-x right" to next-buffer
	  ;([?\s-x right] . next-buffer)
	  ;; Bind "<s-left> to custom function exwm-worskpace-switch-previous"
	  ([s-left] . exwm-workspace-switch-previous)
	  ;; Bind "<s-right> to custom function exwm-worskpace-switch-next"
	  ([s-right] . exwm-workspace-switch-next)
	  ;; Bind "s-0" to "s-9" to switch workspace by index
	  ,@(mapcar (lambda (index)
		      `(,(kbd (format "s-%d" index)) .
			(lambda ( )
			  (interactive)
			  (exwm-workspace-switch-create ,index))))
		    (number-sequence 0 9))
	  ;; Bind "s-&" to launch applications ('M-&' also works if the output
          ;; buffer does not bother you).
          ([?\s-&] . exwm-run-shell-command)))
  ;; Remap "<s-kp> digit keys to s digits equivalents"
  (loop as y below 10 do
	(define-key key-translation-map
	  (kbd (format "<s-kp-%d>" y))
	  (kbd (format "s-%d" y))))
  ;; line-mode keybindings
  (define-key exwm-mode-map [?\C-q] #'exwm-input-send-next-key)
  ;; The following example demonstrates how to use simulation keys to mimic
  ;; the behavior of Emacs.  The value of `exwm-input-simulation-keys` is a
  ;; list of cons cells (SRC . DEST), where SRC is the key sequence you press
  ;; and DEST is what EXWM actually sends to application.  Note that both SRC
  ;; and DEST should be key sequences (vector or string).
  (setq exwm-input-simulation-keys
	'(;; movement
          ([?\C-b] . [left])
          ([?\M-b] . [C-left])
          ([?\C-f] . [right])
          ([?\M-f] . [C-right])
          ([?\C-p] . [up])
          ([?\C-n] . [down])
          ([?\C-a] . [home])
          ([?\C-e] . [end])
          ([?\M-v] . [prior])
          ([?\C-v] . [next])
          ([?\C-d] . [delete])
          ([?\C-k] . [S-end delete])
          ;; cut/paste.
          ([?\C-w] . [?\C-x])
          ([?\M-w] . [?\C-c])
          ([?\C-y] . [?\C-v])
          ;; search
          ([?\C-s] . [?\C-f])))
  ;; Finally enable exwm
  (exwm-enable))
