;;; -*- lexical-binding: t -*-

(cl-eval-when (:compile-toplevel)
  (add-to-list 'load-path (expand-file-name (file-name-as-directory (file-name-concat ".." "init.d"))
					    default-directory))
  (require 'compile-util)
  (load "functions"))

(defvar sly-net-processes)
(defvar sly-default-lisp)
(defvar inferior-lisp-program)
(defvar sly-lisp-implementations)

(declare-function sly-setup nil)
(declare-function sly-mrepl nil)
(declare-function sly-scratch-buffer nil)
(declare-function sly-connect nil)
(declare-function user-git-repository-directory "environment")

(defvar init-sly-systems '((:stumpwm . 3009) (:nyxt . 6009))
  "Alist of common lisp systems and their default ports, used by
  `init-sly-connect' for reference")

(cl-defun init-sly-connect (&key (system :stumpwm) (host "localhost") (port (cdr (assq system init-sly-systems))))
  "Function mean to be called from a common lisp system so sly connects to it

Key argument SYSTEM is a keyword that is searched on `init-sly-systems'. A
matching entry will also give the port to use

Key arguments HOST and PORT must be specified when connecting to
remote systems, PORT must always be specified when connecting to a
system without an entry in `init-sly-systems'

Normally this function will be called from a common lisp system at init if they
find an emacs daemon with a particular socket running, those already should have
a matching entry in `init-sly-systems', and this function is meant to facilitate
the elisp side of the process"
  (when port
    (unless (cl-member port (mapcar 'sly-connection-port sly-net-processes) :test '=)
      (sly-connect host port))))

(defun init-sly-buffer-choice ( )
  "Return a *sly-scratch* buffer in non interactive instances and a mrepl in
  interactive ones

These are the Common Lisp equivalents of *scratch* and *repl* buffers returned
by `initial-buffer-choice'"
  (with-current-buffer (sly-scratch-buffer)
    (cond (noninteractive (current-buffer))
	  (t (let ((display-buffer-overriding-action '(display-buffer-no-window ((allow-no-window . t)))))
	       (sly-mrepl))))))

(add-to-list 'initial-buffer-choice-functions 'init-sly-buffer-choice)

;; Set default CL implementation
(cl-loop as lisp in '(sbcl ecl clisp ccl cmucl)
	 as route = (executable-find (symbol-name lisp))
	 when (and route (file-executable-p route))
	 do (setf sly-default-lisp lisp
		  inferior-lisp-program route)
	 and return t)

(let ((init-file (expand-file-name "init.lisp" (expand-file-name "common-lisp" (getenv "XDG_CONFIG_HOME")))))
  (setf sly-lisp-implementations
	`((ecl ("ecl" "--norc" "--load" ,init-file))
	  (ccl ("ccl"))
	  (clisp ("clisp" "-q"))
	  (cmucl ("cmucl" "-quiet"))
	  (sbcl ("sbcl" "--noinform" "--userinit" ,init-file)
		:coding-system utf-8-unix))))

(sly-setup) ;; Sly
(add-to-list 'init-coding-modes-hooks 'sly-mrepl-mode-hook)

(when (file-directory-p (expand-file-name "nyxt/" (user-git-repository-directory)))
  (load-configuration "nyxt"))
