;;; -*- lexical-binding: t -*-

(eval-when-compile
  (require 'compile-util))

(add-to-list 'init-coding-modes-hooks 'ielm-mode-hook)

(setq ielm-prompt (format "%s> " (upcase user-login-name)))
