;;; -*- lexical-binding: t -*-
					;(insert (propertize "goddess" 'face '(:foreground "BlueViolet" :weight "bold")))

(cl-eval-when (:compile-toplevel)
  (require 'dom)
  (require 'nov))

(defface init-nov-render-default
  '((default . (:family "Fira Code" :width normal :height 1.16 :weight normal
			:slant normal :foreground "#ae81ff" :underline nil :overline nil)))
  "The default face for text" :group 'nov)

(defface init-nov-render-dialog
  '((default . (:inherit init-nov-render-default :foreground "#6dfedf")))
  "Face used for anything within \"\" marks, and similar" :group 'nov)

(defface init-nov-render-italics
  '((default . (:inherit init-nov-render-default :slant italic :foreground "#ff79c6")))
  "Face used for italics and emphasis" :group 'nov)

(defface init-nov-render-bold
  '((default . (:inherit init-nov-render-default :weight bold :foreground "#2de0a7")))
  "Face used for bold text" :group 'nov)

(defface init-nov-render-underline
  '((default . (:inherit init-nov-render-default :underline t)))
  "Face used for underlined text" :group 'nov)

(defface init-nov-render-hr 
  '((default . (:inherit init-nov-render-default :foreground "#ff79c6")))
  "Face used for hr tags" :group 'nov)

(defface init-nov-render-title
  '((default . (:inherit init-nov-render-default :weight semi-bold :foreground "#6dfedf")))
  "Face used for titles" :group 'nov)

(defface init-nov-render-link
  '((default . (:inherit init-nov-render-default :foreground "#efe4a1")))
  "Face used for buttons and links" :group 'nov)

(defvar init-nov-render-styles '((:default . (face init-nov-render-default))
				 (:dialog . (face init-nov-render-dialog))
				 (:emphasis . (face init-nov-render-italics))
				 (:bold . (face init-nov-render-bold))
				 (:underline . (face init-nov-render-underline))
				 (:horizontal-rule . (face init-nov-render-hr))
				 (:title . (face init-nov-render-title)))
  "PList of text properties to add to text on render")

(define-button-type 'init-nov-render-button
  'action 'nov-browse-url 'face 'init-nov-render-link 'help-echo "Browse this link" 'follow-link t 'button t)

(defun init-nov-setup-auto-adjust ( )
  (add-hook 'window-configuration-change-hook 'init-nov-adjust-text 0 t))

(add-hook 'nov-mode-hook 'init-nov-setup-auto-adjust)
(bind-key "j" 'init-nov-adjust-text 'nov-mode-map)

(defun init-nov-adjust-margins ( )
  "Adjust margins for each window showing the story during redisplays"
  (let ((margins (window-margins))
	(size (ceiling (/ (* 3.0 (window-max-chars-per-line)) 100))))
    (when (or (null (car margins))
	      (not (= (car margins) size)))
      (setf left-margin-width size
	    right-margin-width size)
      (set-window-margins (selected-window) size size))))

(defun init-nov-adjust-text ( )
  "Adjust text for each window showing the story during redisplays"
  (interactive)
  (let* ((inhibit-read-only t)
	 (width (window-max-chars-per-line (selected-window) 'init-nov-render-default))
	 (size (round (/ (* 3.0 width) 100))))
    (setf fill-column width)
    (set-window-margins (selected-window) size size)
    (fill-region (point-min) (point-max) 'full nil t)))

(defun init-nov-dialogs-in-string (string &optional pending-dialog)
  "Return a list of the boundaries of each dialog present in string STRING in the form (((begining end) ...) . pending-dialog), where pending dialog is a boolean for wether a dialog was left inconcluse in the string

Optional argument PENDING-DIALOG means that STRING starts with an inconcluse dialog, and should be set to what a previous invocation of this function returned for that argument, when determining the boundaries of dialogs in a paragraph, see `init-nov-dialogs-in-paragraph'"
  (cl-loop with match = 0
	   with range
	   with done
	   while (not done)
	   when (cond ((and pending-dialog
			    (not (string-match "^.*?[\">”]" string match)))
		       (setf range (list 0 (length string))
			     done t))
		      ((and pending-dialog
			    (string-match "^.*?[\">”]" string match))
		       (setf pending-dialog nil
			     match (match-end 0)
			     range (match-data)))
		      ((string-match "\\(?:\\(\"\\).*?\\1\\|<.*?>\\|“.*?”\\)"
				     string match)
		       (setf match (match-end 0)
			     range (match-data)))
		      ((string-match "[\"<“].*?$" string match)
		       (setf pending-dialog t
			     range (match-data)
			     done t))
		      (t (setf done t) nil))
	   collect range into ranges
	   finally return (cons ranges pending-dialog)))

(defun init-nov-dialogs-in-element (element)
  "For each toplevel string in element, determine the dialog boundaries. String with no apparent dialogs but between inconcluse dialogs in surrounding strings are trated as pure dialogs.

For example, in \"This “is\" \"a sentence\" \"and a following oration”\" the string \"a sentence\" is considered part of the dialog that span accross the surrounding strings \"This “is\" and \"and a following oration”\"

Returns a list made up of the results from `init-nov-dialogs-in-string'"
  (cl-loop as string in (cl-remove-if-not #'stringp (dom-children element))
	   as (dialogs . pending-dialog) = (init-nov-dialogs-in-string string)
	   then (init-nov-dialogs-in-string string pending-dialog)
	   collecting dialogs))

(defun init-nov-style-attr-to-plist (attr)
  (when attr
    (setf attr (split-string attr ";"))
    (mapcar (lambda (string)
	      (pcase (split-string string ":")
		(`(,property ,value)
		 (cons property value))))
	    attr)))

(defun init-nov-renderize-string (string &optional style dialogs)
  (unless style (setf style :default))
  (let ((content string))
    (setf content (apply #'propertize content (cdr (assq style init-nov-render-styles))))
    ;; Hightlight dialogues
    (cl-loop as (beg end) in dialogs
	     do (add-text-properties beg end (cdr (assq :dialog init-nov-render-styles)) content))
    (insert content)))

(cl-labels ((render (element &optional (toplevel t))
	      (pcase (dom-tag element)
		('p 
		 (recurse element :default toplevel)
		 (newline 2))
		((or 'i 'em 'sup 'sub)
		 (recurse element :emphasis toplevel))
		((or 'b 'strong)
		 (recurse element :bold toplevel))
		('u
		 (recurse element :underline toplevel))
		((and 'span (let style (init-nov-style-attr-to-plist (dom-attr element 'style))))
		 (recurse element (or (and (string= "bold" (alist-get "font-style" style ( ) ( ) #'string=)) :bold)
				      (and (string= "italic" (alist-get "font-style" style ( ) ( ) #'string=)) :emphasis)
				      :default)
			  toplevel))
		('hr
		 (newline) (render-horizontal-rule) (newline))
		('br (newline))
		((or 'h1 'h2 'h3 'h4 'h5 'h6) (init-nov-renderize-string (dom-text element) :title))
		('table
		 (newline)
		 (render-horizontal-rule)
		 (cl-loop as child in (dom-children element)
			  do (recurse child)
			  (render-horizontal-rule))
		 (newline))
		('tr
		 (recurse element)
		 (newline))
		('img
		 (nov-render-img element))
		('a
		 (insert-text-button (or (dom-text element) (dom-attr element 'href))
				     'type 'init-nov-render-button
				     'shr-url (dom-attr element 'href)))
		('li
		 (cl-loop as element in (dom-children element)
			  do (if (stringp element)
				 (init-nov-renderize-string element)
			       (init-nov-render-html-dom element)))
		 (newline 2))
		((or 'div 'body 'ol 'ul 'blockquote 's 'section 'nav 'td)
		 (recurse element :default toplevel))
		(_
		 (warn "init-nov-render-html-dom: unhandled element %s" element))))
	    (recurse (dom &optional style toplevel)
	      (cl-loop as child in (dom-children dom)
		       with dialogs = (when toplevel (init-nov-dialogs-in-element dom))
		       do (if (stringp child)
			      (init-nov-renderize-string child style (and toplevel (pop dialogs)))
			    (render child toplevel))))
	    (render-horizontal-rule (&optional (font 'init-nov-render-hr) (style :horizontal-rule))
	      (init-nov-renderize-string (make-string (window-max-chars-per-line (selected-window)
										 font)
						      ?-)
					 style)
	      (newline)))
  (cl-defun init-nov-render-html-dom (element)
    "Render dom. String are relegated to `init-nov-renderize-string'

Proper elements, such as div, are recursed

Others, such as img, are handled directly

Warn if an unkonw element is encountered"
    (render element t)))

(defun init-nov-render-html (dom)
  "Render the actual chapters"
  (init-nov-render-html-dom (dom-by-tag dom 'body))
  (save-excursion
    (goto-char (point-min))
    (while (re-search-forward "\n\\{3,\\}" nil t)
      (replace-match "\n\n")))
  (setf word-wrap t)
  (init-nov-adjust-text))

(defun init-nov-render ( )
  "Render the content of epub novels"
  (let ((dom (libxml-parse-html-region (point-min) (point-max))))
    (run-hooks 'nov-pre-html-render-hook)
    (erase-buffer)
    (init-nov-render-html dom)
    (run-hooks 'nov-post-html-render-hook)))

;; (defun init-nov-render-ncx-toc (dom)
;;   "Render the ncx variant of tocs"
;;   (let ((dom (libxml-parse-html-region (point-min) (point-max))))
;;     (erase-buffer)))

(setf nov-render-html-function 'init-nov-render)

(let (timers)
  (cl-defun nov-update-place (&rest args &key (delay 60) &allow-other-keys)
    "Update index and position in `nov-save-place-file' via `nov-save-place' lazily.

This function relies on being in a nov buffer to work correctly, that buffer is remembered by the timer,
and if it isn't live by the time it runs, nothing happens.

After DELAY seconds of idle time, commit the updated state

Returns the timer created, or nil if a timer is already scheduled for the given buffer"
    (cl-flet ((lambda (context)
		(unwind-protect
		    (when (and (buffer-live-p context)
			       (provided-mode-derived-p (buffer-local-value 'major-mode context) 'nov-mode))
		      (with-current-buffer context
			(let ((coding-system-for-read 'no-conversion)
			      (coding-system-for-write 'no-conversion))
			  (nov-save-place (alist-get 'identifier nov-metadata) nov-documents-index (point)))))
		  (setf (alist-get context timers ( ) t) ( )))))
      (unless (alist-get (current-buffer) timers)
	(setf (alist-get (current-buffer) timers)
	      (run-with-idle-timer delay ( ) #'lambda (current-buffer)))))))

(cl-loop as function in '(nov-goto-document nov-scroll-up nov-scroll-down)
	 do (advice-add function :after 'nov-update-place))

(defun init-disable-mode-line ( )
  (setf mode-line-format nil))

(add-hook 'nov-mode-hook 'init-disable-mode-line)
