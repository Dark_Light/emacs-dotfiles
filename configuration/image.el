;;; -*- lexical-binding: t -*-

(require 'image-converter)

(defun image-webp-to-png (image datap)
  (let* ((transient (when datap (make-temp-file "webp-image-" ( ) "-source" image)))
	 (input (if datap transient image))
	 (error-output (generate-new-buffer "*dwebp-error-output*" t))
	 process error-process)
    (unwind-protect
	(progn
	  (setf process (make-process :name "dwebp" :buffer (current-buffer)
				      :command `("dwebp" ,input "-mt" "-o" "-")
				      :connection-type 'pipe
				      :stderr error-output :sentinel 'ignore)
		error-process (get-buffer-process error-output)
		(process-sentinel error-process) 'ignore)
	  (while (accept-process-output process))
	  (while (accept-process-output error-process))
	  (let ((status (process-exit-status process)))
	    (unless (and (numberp status)
			 (zerop status))
	      (error "unable to convert webp image to png: %s"
		     (with-current-buffer error-output (buffer-string)))))
	  t)
      (when transient (delete-file transient))
      (when (buffer-live-p error-output) (kill-buffer error-output)))))

(image-converter-add-handler "webp" 'image-webp-to-png)

;; :filter (lambda (process output)
;; 	  (when (buffer-live-p (get-buffer "goldy"))
;; 	    (with-current-buffer (get-buffer "goldy")
;; 	      (goto-char (process-mark process))
;; 	      (insert output)
;; 	      (set-marker (process-mark process) (point)))))

;; (process-buffer process)
;; "/gnu/store/zw90b817cgm72lr29n4fld9ln8d0bmph-libwebp-1.2.2/bin"
;; "-quiet"
