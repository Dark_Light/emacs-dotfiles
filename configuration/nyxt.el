;;; -*- lexical-binding: t -*-

(load (expand-file-name "nyxt/build-scripts/nyxt-guix.el" (user-git-repository-directory)) t t)

(setf nyxt-guix-profile-directory (substitute-in-file-name "$XDG_CONFIG_HOME/guix/temporal-profiles/nyxt"))

(add-to-list 'sly-lisp-implementations
	     `(sbcl-nyxt ,(lambda ( )
			    (nyxt-make-guix-sbcl-for-nyxt
			     (expand-file-name "nyxt/" (user-git-repository-directory))))))
