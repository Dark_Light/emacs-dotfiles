;;; -*- lexical-binding: t -*-

(require 'smartparens-config)

(cl-eval-when (:compile-toplevel)
  (add-to-list 'load-path (expand-file-name "init.d" user-emacs-directory))
  (require 'variables) (require 'rx) (require 'cl-lib))

(defun init-setup-smartparens ( )
  (dolist (coding-mode init-coding-modes-hooks)
    (add-hook coding-mode #'smartparens-mode))
  (cl-loop as buffer in (buffer-list)
	   with coding-modes = (cl-loop as mode in init-coding-modes-hooks
					collecting (intern
						    (replace-regexp-in-string (rx "-hook" line-end)
									      ""
									      (symbol-name mode))))
	   when (and (member (buffer-local-value 'major-mode buffer) coding-modes)
		     (not (member 'smartparens-mode (buffer-local-value 'local-minor-modes buffer))))
	   do (with-current-buffer buffer (smartparens-mode))))

(run-at-time 0 ( ) #'init-setup-smartparens)

(sp-local-pair 'minibuffer-mode "'" ( ) :actions ( ))

(with-eval-after-load 'sly
  (sp-local-pair 'sly-mrepl-mode "'" ( ) :actions ( )))
