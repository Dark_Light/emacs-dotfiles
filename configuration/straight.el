;;; -*- lexical-binding: t; -*-

(require 'straight)

(defun init-add-local-packages (&rest descriptors)
  "Add custom packages from `user-git-repository-directory'

Arguments should be lists in the form (recipe config add-to-list)
where:

recipe is a recipe acceptable by `straight-use-package' and includes a
:local-repo property, or a symbol

config is the base name for a file in `user-elisp-config-directory',
or any non nil value to use package's name

Returns a list of procesed packages"
  (cl-loop as entry in descriptors
	   as (template config) = (if (listp entry) entry (list entry))
	   as location = (expand-file-name (file-name-as-directory (cl-etypecase template
								     (symbol (symbol-name template))
								     (cons (alist-get :local-repo template))))
					   (user-git-repository-directory))
	   as recipe = (cl-etypecase template
			 (cons template)
			 (symbol `(,template :type git :local-repo ,location)))
	   when (file-directory-p location) do (straight-use-package recipe)
	   when (and config (not (memq :load-configuration recipe)))
	   do (nconc recipe `(:load-configuration ,config))
	   collecting (car recipe)))
