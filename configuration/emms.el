;;; -*- lexical-binding: t -*-

(cl-eval-when (compile)
  (require 'xdg)
  (require 'time-date)
  (require 'emms-setup)
  (require 'emms-source-file)
  (require 'emms-browser)
  (require 'emms-history))

;;; Start
(emms-all)

;;; Sources and Players
(setf emms-source-file-default-directory (xdg-user-dir "MUSIC")
      emms-setup-default-player-list (cl-loop as entry in emms-setup-default-player-list
					      as player = (symbol-name entry)
					      when (string-match-p (rx (*? anything)
								       (or "mpv" "vlc")
								       (*? anything))
								   player)
					      collecting entry))

;;; add music collection sources
(emms-add-directory-tree emms-source-file-default-directory)

;;; make sure local sources are looked up portably
(setf emms-source-file-directory-tree-function 'emms-source-file-directory-tree-internal)

;;; covers
(setf emms-browser-thumbnail-small-size 64
      emms-browser-thumbnail-medium-size 128
      emms-browser-covers #'emms-browser-cache-thumbnail-async)

;;; filters
;; (emms-browser-make-filter "all" #'ignore)
;; (emms-browser-make-filter "recent"
;; 			  (lambda (track) (< 30
;; 					     (time-to-number-of-days
;; 					      (time-subtract (current-time)
;; 							     (emms-info-track-file-mtime track))))))
(emms-browser-set-filter (assoc "all" emms-browser-filters))

;;; history
(emms-history-load)

;;; Play
(setq-default
 emms-source-playlist-default-format 'm3u
 emms-playlist-mode-center-when-go t
 emms-playlist-default-major-mode 'emms-playlist-mode
 emms-show-format "%s"
 emms-repeat-playlist t

 emms-player-mpv-environment '("PULSE_PROP_media.role=music")
 emms-player-mpv-parameters '("--quiet" "--really-quiet" "--no-audio-display" "--force-window=no" "--vo=null"))
