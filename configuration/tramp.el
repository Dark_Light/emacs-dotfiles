;;; -*- lexical-binding: t -*-

(require 'tramp)

(setf tramp-default-user (user-login-name) ;; Catch All User
      tramp-default-host (system-name) ;; Catch All Host
      tramp-default-method "ssh" ;; Catch All Method
      tramp-remote-path (cl-loop as entry in tramp-remote-path
				 collecting (if (and (stringp entry)
						     (string-match-p (rx line-start "~" (* anything))
								     entry))
						(replace-regexp-in-string (rx "~") "$HOME" entry)
					      entry))
      tramp-encoding-shell (executable-find "sh"))

(connection-local-set-profile-variables 'tramp-guix-environment
					'((tramp-remote-path . (tramp-own-remote-path))))

(connection-local-set-profile-variables 'tramp-android-environment
					'((temporary-file-directory . "/data/data/com.termux/files/usr/tmp")
					  (tramp-remote-path . (tramp-own-remote-path))))


(connection-local-set-profiles '(:application tramp :machine "mooncat") 'tramp-guix-environment)
(connection-local-set-profiles '(:application tramp :machine "darkcat") 'tramp-android-environment)

(add-to-list 'tramp-connection-properties '("/sshfs:mooncat:" "~user" "/home/user"))
(add-to-list 'tramp-connection-properties '("/sshfs:darkcat:" "~" "/data/data/com.termux/files/home"))
(add-to-list 'tramp-connection-properties '("/sshfs:darkcat:" "tmpdir" "/data/data/com.termux/files/usr/tmp"))
(add-to-list 'tramp-connection-properties '("/ssh:darkcat:" "tmpdir" "/data/data/com.termux/files/usr/tmp"))
