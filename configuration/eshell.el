;;; -*- lexical-binding: t -*-

(eval-when-compile
  (require 'compile-util))

(eval-and-compile
  (require 'eshell) ;; And eshell for that matter
  (require 'esh-var)
  (require 'esh-mode)
  (require 'esh-module))

(add-to-list 'eshell-modules-list 'eshell-tramp t)
(add-to-list 'init-coding-modes-hooks 'eshell-mode-hook)

;; Eshell doesn't support escape codes, at all
(when (not (string= (getenv "TERM") "dumb"))
  (setenv "TERM" "dumb"))

;; On interactive emacs always keep at least one eshell session active
(unless noninteractive (add-hook 'after-init-hook 'eshell-regenerate-session 60))

;; (defvar eshell-buffer-name)

(defun eshell-regenerate-session (&rest _)
  "Create a persistent eshell buffer if an eshell session named
  `eshell-buffer-name' doesn't already exists.

This works by creating a buffer with `eshell-buffer-name' as name,
setting `eshell-mode' on it, and then adding this function to it's
buffer local `after-kill-buffer-functions' to bring it back right
after exiting.

Returns the newly created session or nil if a session already exists"
  (unless (get-buffer eshell-buffer-name)
    (let ((session (generate-new-buffer eshell-buffer-name)))
      (with-current-buffer session
	(unless (derived-mode-p 'eshell-mode)
	  (eshell-mode))
	(add-hook 'after-kill-buffer-functions 'eshell-regenerate-session 0 t))
      session)))

(defmacro eshell-command-to-string (command)
  "Return the output of `eshell-command' as a string"
  `(with-temp-buffer
     (let ((standard-output (current-buffer)))
       (eshell-command ,command t)
       (buffer-substring-no-properties (point-min) (point-max)))))

(defun eshell/picolisp (&rest args)
  (let ((root (replace-regexp-in-string "bin/picolisp" "" (file-truename (executable-find "picolisp")))))
    (throw 'eshell-replace-command
	   (eshell-parse-command "*picolisp" (append (list (expand-file-name "lib.l" root)
							   (expand-file-name "ext.l" root))
						     (eshell-stringify-list (flatten-tree args)))))))
