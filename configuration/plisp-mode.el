;;; -*- lexical-binding: t -*-

(defun init-plisp-repl ( )
  "Same as `plisp-repl', but without certain issues"
  (interactive)
  (let* ((root init-picolisp-directory)
	 (lib (expand-file-name "lib.l" root))
	 (ext (expand-file-name "ext.l" root)))
    (switch-to-buffer (make-comint "picolisp-repl" plisp-picolisp-executable ( ) lib ext))
    (plisp-repl-mode)))

(setf plisp-pil-executable (file-truename (executable-find "pil"))
      plisp-picolisp-executable (file-truename (executable-find "picolisp")))

(defvar init-picolisp-directory
  (when-let* ((picolisp (executable-find "picolisp"))
	      (picolisp-directory (file-name-as-directory
				  (cl-case init-system-type
				    (guix (or (replace-regexp-in-string (rx "bin/picolisp" eol) ""
									(file-truename picolisp))
					      ""))
				    (termux (file-name-concat (getenv "PREFIX") "lib" "picolisp"))
				    (t (replace-regexp-in-string "bin" "lib" picolisp))))))
    (and (file-directory-p picolisp-directory) picolisp-directory))
  "The picolisp directory, set to non-nil when found")

(when init-picolisp-directory
  (setf plisp-pilindent-executable (expand-file-name "bin/pilIndent" init-picolisp-directory)
	plisp-documentation-unavailable ( )
	plisp-documentation-directory (expand-file-name "doc/" init-picolisp-directory)))
