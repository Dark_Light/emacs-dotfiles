;;; -*- lexical-binding: t -*-
(defadvice xwidget-webkit-callback (after xwidget-webkit-functions activate)
  (xwidget-webkit-execute-script
   (xwidget-webkit-current-session)
   "function Clamp ( num , min , max ) {
    if ( min > max ) { [ min , max ] = [ max , min ] }
    return num <= min ? min : num >= max ? max : num;
}

function IsHTMLElement ( Element ) {
    \"use strict\";
    return ( Element && /html.*element/.test ( TypeOf ( Element ) ) );
}

function ViewSize ( ) {
    \"use strict\";
    // This algorithm avoids creating test page to determine if document.documentElement.client[Width|Height] is greater then view size,
    // will succeed where such test page wouldn't detect dynamic unreliability,
    // and will only fail in the case the right or bottom edge is within the width of a scrollbar from edge of the viewport that has visible scrollbar(s).
    const body = document.body, doc = ( document.documentElement || document.body.parentNode || body );
    let w, h;
    return ( typeof document.clientWidth === \"number\" ) ? ( { w : document.clientWidth, h : document.clientHeight } ) :
	( ( doc === body )
          || ( w = Math.max ( doc.clientWidth, body.clientWidth ) > self.innerWidth )
          || ( h = Math.max ( doc.clientHeight, body.clientHeight ) > self.innerHeight ) ?
	  { w : body.clientWidth, h : body.clientHeight } : { w : w, h : h } );
}

function GetPos ( Target = \"self\", POV = \"page\" ) {
    if ( Target === \"self\" ) {
	return ( { x : document.body.scrollLeft, y : document.body.scrollTop } );
    } else {
	if ( ! IsHTMLElement ( Target ) || Target.nodeType !== Node.ELEMENT_NODE ) { return { x : 0, y : 0 } };
	let Offset = { x : 0, y : 0 };
	while ( Target )
	{
	    Offset.x += Target.offsetLeft;
	    Offset.y += Target.offsetTop;
	    Target = Target.offsetParent;
	}
	
	switch ( POV ) {
	case \"page\":
	    return ( Offset );
	    break;
	case \"window\":
	    if ( document.body && ( document.body.scrollTop || document.body.scrollLeft ) )
	    {
		Offset.x -= document.body.scrollLeft;
		Offset.y -= document.body.scrollTop;
	    }
	    else if ( window.pageXOffset || window.pageYOffset )
	    {
		Offset.x -= window.pageXOffset;
		Offset.y -= window.pageYOffset;
	    }
	    else if ( document.documentElement && ( document.documentElement.scrollTop || document.documentElement.scrollLeft ) )
	    {
		Offset.x -= document.documentElement.scrollLeft;
		Offset.y -= document.documentElement.scrollTop;
	    }
	    return ( Offset );
	    break;
	}
    }
}

function IsInWindow ( element, POV = \"window\" ) {
    if ( ! IsHTMLElement ( element ) || element.nodeType !== Node.ELEMENT_NODE ) { return ( false ) }
    const ViewHeight = ViewSize ( ).h, ScrollHeight = element.scrollHeight;
    const Position = ( GetPos ( element, \"window\" ).y );
    switch ( POV ) {
    case \"top\":
	return ( ( Position <= 0 ) && ( ( ScrollHeight + Position ) >= 0 ) );
	break;
    case \"bottom\":
	return ( ( Position <= ViewHeight ) && ( ( ScrollHeight + Position ) >= ViewHeight ) );
	break;
    case \"window\":
	return ( ( Position <= ViewHeight ) && ( ( ScrollHeight + Position ) >= 0 ) );
    }
}

function SmoothScrollBy ( X, Y ) {
    \"use strict\";
    if ( typeof X !== \"number\" || ! isFinite ( X ) || typeof Y !== \"number\" || ! isFinite ( Y ) ) {
	return;
    }
    //If it's small enough, scroll in one go
    let distanceX = Math.abs ( X ), distanceY = Math.abs ( Y );
    let distance = ( distanceX + distanceY ), trivial = ( distance < 100 );
    if ( trivial ) {
	scrollBy ( X, Y );
	return;
    }
    //How far can we really scroll?
    const Pos = GetPos ( ), ViewPort = ViewSize ( );
    const offsetX = ( Pos.x + ( ( X >= 0 ) ? ( ViewPort.w ) : ( 0 ) ) );
    const offsetY = ( Pos.y + ( ( Y >= 0 ) ? ( ViewPort.h ) : ( 0 ) ) );
    const limitX = ( ( X >= 0 ) ? ( document.body.scrollWidth - offsetX ) : ( offsetX ) );
    const limitY = ( ( Y >= 0 ) ? ( document.body.scrollHeight - offsetY ) : ( offsetY ) );
    X = Math.min ( distanceX, limitX ) * ( ( X >= 0 ) ? ( 1 ) : ( -1 ) );
    Y = Math.min ( distanceY, limitY ) * ( ( Y >= 0 ) ? ( 1 ) : ( -1 ) );
    let stepX = Math.round ( X / 10 ), stepY = Math.round ( Y / 10 ), leapt = 1;
    const cyclesX = ( ( ( stepX ) != 0 ) ? ( Math.abs ( X / stepX ) ) : ( 0 ) );
    const cyclesY = ( ( ( stepY ) != 0 ) ? ( Math.abs ( Y / stepY ) ) : ( 0 ) );
    const cycles = ( cyclesX + cyclesY );
    const animate = function ( ) {//This handles scrolling proper
	if ( stepX != 0 && leapt > cyclesX ) {
	    stepX = 0;
	} else if ( stepY != 0 && leapt > cyclesY ) {
	    stepY = 0;
	}
	if ( leapt <= cycles ) {
	    scrollBy ( stepX, stepY );
	    leapt += 1;
	    requestAnimationFrame ( animate );
	} else {
	    scrollBy ( restX, restY );
	}
    }
    //Animate the scrolling with frame animations
    const restX = Math.round ( ( cyclesX % 1 ) * stepX ), restY = Math.round ( ( cyclesY % 1 ) * stepY );
    requestAnimationFrame ( animate );
}

function ScrollByPages ( quantity ) {
    \"use strict\";
    if ( typeof quantity === \"number\" && isFinite ( quantity ) ) {
	SmoothScrollBy ( 0, ( Math.round ( ViewSize( ).h * 0.875 ) * Math.round ( quantity ) ) )
    } else {
	return;
    }
}

function ScrollIntoElement ( Element ) {
    \"use strict\";
    const Pos = GetPos ( Element, \"window\" );
    SmoothScrollBy ( Pos.x, Pos.y )
}"))

(defun xwidget-debug ( command )
  (xwidget-webkit-execute-script
   (xwidget-webkit-current-session)
   command (lambda ( log )
	     (print log))))

(defun xwidget-webkit-scroll-page-up ()
  "Scroll webkit up by one page. Page is the client height"
  (interactive)
  (xwidget-webkit-execute-script
   (xwidget-webkit-current-session)
   "self.ScrollByPages ( -1 );"))

(defun xwidget-webkit-scroll-page-down ()
  "Scroll webkit down by one page. Page is the client height"
  (interactive)
  (xwidget-webkit-execute-script
   (xwidget-webkit-current-session)
   "self.ScrollByPages ( 1 );"))

(defun xwidget-webkit-forward ()
  "Go forward in history."
  (interactive)
  (xwidget-webkit-execute-script
   (xwidget-webkit-current-session)
   "history.go ( 1 );"))
