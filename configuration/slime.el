;;; -*- lexical-binding: t -*-
(defvar inferior-lisp-program)
(defvar slime-default-lisp)
(defvar slime-lisp-implementations)

(let ((eval-option (list "--eval" "(set-aeryn-package)"))
      (init-file (expand-file-name "init.lisp" (expand-file-name "common-lisp" (getenv "XDG_CONFIG_HOME")))))
  (setf slime-lisp-implementations
	`((ecl ("ecl" "--norc" "--load" ,init-file ,@eval-option))
	  (ccl ("ccl"))
	  (clisp ("clisp" "-q"))
	  (cmucl ("cmucl" "-quiet"))
	  (sbcl ("sbcl" "--noinform" "--userinit" ,init-file ,@eval-option)
		:coding-system utf-8-unix))))

;; Find CL implementation by preference
;; Set default CL implementation
(cl-loop as y in '(sbcl ecl clisp ccl cmucl)
	 as route = (executable-find (symbol-name y))
	 when (and route (file-executable-p route))
	 do (setf slime-default-lisp y
		  inferior-lisp-program route)
	 and return t)

(slime-setup)
(slime-auto-start)
(add-to-list 'init-coding-modes-hooks 'slime-repl-mode-hook)
