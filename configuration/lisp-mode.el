;;; -*- lexical-binding: t -*-

(eval-when-compile
  (require 'compile-util))

(add-to-list 'init-coding-modes-hooks 'lisp-mode-hook)
(add-to-list 'init-coding-modes-hooks 'lisp-interaction-mode-hook)

(setq emacs-lisp-docstring-fill-column 70)
