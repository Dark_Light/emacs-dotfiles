* Description
This is my emacs base configuration, whose layout emphazises modularity and portability.

Always assume the latest emacsen


* Features
** It is designed to run in all three standalone, daemon and batch modes
** It also has a modular design, and the ability to auto (re)compile itself
** Uses straight and use-package for package management and configuration


* To Do 
** Ensure Windows portability


* Notes
** On first run, straight will be busy setting itself up and installing the packages
** and this happens syncronously, expect emacs to freeze for a while...
