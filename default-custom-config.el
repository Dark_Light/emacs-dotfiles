;; custom-config.el --- Custom Config File -*- lexical-binding: t -*-
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;Custom;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(auto-save-default ( ))
 '(blink-cursor-blinks 30)
 '(blink-cursor-interval 0.2)
 '(browse-url-browser-function 'init-url-browser-function)
 '(buffer-face-mode-face ( ))
 '(cursor-type '(hbar . 1))
 '(custom-safe-themes t)
 '(delete-active-region ( ))
 '(dired-listing-switches "-Dhial")
 '(echo-keystrokes 0.3)
 '(eshell-history-size 6000)
 '(eshell-prefer-lisp-functions t)
 '(eshell-prefer-lisp-variables t)
 '(eval-expression-print-length ( ))
 '(eval-expression-print-level ( ))
 '(fringe-mode 10 ( ) (fringe))
 '(gc-cons-threshold 3145728)
 '(image-use-external-converter t)
 '(inhibit-startup-screen t)
 '(initial-buffer-choice 'initial-buffer-choice)
 '(initial-scratch-message ( ))
 '(large-file-warning-threshold 536870912 ( ) ( ) "512 MiB")
 '(linum-format " %6d ")
 '(load-prefer-newer t)
 '(main-line-color1 "#222912")
 '(main-line-color2 "#09150F")
 '(make-backup-files ( ))
 '(mouse-drag-copy-region t)
 '(mouse-yank-at-point t)
 '(org-html-doctype "html5")
 '(org-html-html5-fancy t)
 '(package-enable-at-startup ( ))
 '(safe-local-variable-values
   '((eval let
	   ((root-dir-unexpanded
	     (locate-dominating-file default-directory ".dir-locals.el")))
	   (when root-dir-unexpanded
	     (let*
		 ((root-dir
		   (expand-file-name root-dir-unexpanded))
		  (root-dir*
		   (directory-file-name root-dir)))
	       (unless
		   (boundp 'geiser-guile-load-path)
		 (defvar geiser-guile-load-path '( )))
	       (make-local-variable 'geiser-guile-load-path)
	       (require 'cl-lib)
	       (cl-pushnew root-dir* geiser-guile-load-path :test #'string-equal))))
     (eval setq-local guix-directory
	   (locate-dominating-file default-directory ".dir-locals.el"))
     (eval modify-syntax-entry 43 "'")
     (eval modify-syntax-entry 36 "'")
     (eval modify-syntax-entry 126 "'")
     (Syntax . ANSI-Common-Lisp)
     (Base . 10)
     (Package SERIES :use "COMMON-LISP" :colon-mode :external)
     (syntax . ANSI-COMMON-LISP)
     (eval cl-flet
	   ((enhance-imenu-lisp
	     (&rest keywords)
	     (dolist
		 (keyword keywords)
	       (add-to-list 'lisp-imenu-generic-expression
			    (list
			     (purecopy
			      (concat
			       (capitalize keyword)
			       (if
				   (string=
				    (substring-no-properties keyword -1)
				    "s")
				   "es" "s")))
			     (purecopy
			      (concat "^\\s-*("
				      (regexp-opt
				       (list
					(concat "define-" keyword))
				       t)
				      "\\s-+\\(" lisp-mode-symbol-regexp "\\)"))
			     2)))))
	   (enhance-imenu-lisp "bookmarklet-command" "class" "command" "ffi-method" "function" "mode" "parenscript" "user-class"))
     (sly-load-failed-fasl . ask)
     (shell-script-mode . t)
     (Package ITERATE :use "COMMON-LISP" :colon-mode :external)
     (syntax . COMMON-LISP)
     (whitespace-style quote
		       (face trailing empty tabs))
     (whitespace-action)))
 '(select-enable-primary t)
 '(sly-auto-start 'always)
 '(sly-command-switch-to-existing-lisp ''always)
 '(term-bind-key-alist
   '(("C-c C-c" . term-interrupt-subjob)
     ("C-c C-e" . term-send-esc)
     ("C-p" . previous-line)
     ("C-n" . next-line)
     ("C-s" . isearch-forward)
     ("C-r" . isearch-backward)
     ("C-m" . term-send-return)
     ("C-y" . term-paste)
     ("M-f" . term-send-forward-word)
     ("M-b" . term-send-backward-word)
     ("M-o" . term-send-backspace)
     ("M-p" . term-send-up)
     ("M-n" . term-send-down)
     ("M-M" . term-send-forward-kill-word)
     ("M-N" . term-send-backward-kill-word)
     ("<C-backspace>" . term-send-backward-kill-word)
     ("M-r" . term-send-reverse-search-history)
     ("M-d" . term-send-delete-word)
     ("M-," . term-send-raw)
     ("M-." . comint-dynamic-complete)
     ("S-<left>" . multi-term-prev)
     ("S-<right>" . multi-term-next)))
 '(tramp-terminal-type "tramp")
 '(transmission-service 9999)
 '(user-mail-address "bluelightmoon3@gmail.com")
 '(vc-annotate-background ( ))
 '(vc-annotate-color-map
   '((20 . "#d54e53")
     (40 . "goldenrod")
     (60 . "#e7c547")
     (80 . "DarkOliveGreen3")
     (100 . "#70c0b1")
     (120 . "DeepSkyBlue1")
     (140 . "#c397d8")
     (160 . "#d54e53")
     (180 . "goldenrod")
     (200 . "#e7c547")
     (220 . "DarkOliveGreen3")
     (240 . "#70c0b1")
     (260 . "DeepSkyBlue1")
     (280 . "#c397d8")
     (300 . "#d54e53")
     (320 . "goldenrod")
     (340 . "#e7c547")
     (360 . "DarkOliveGreen3")))
 '(vc-annotate-very-old-color ( ))
 '(vc-display-status ( ))
 '(vc-follow-symlinks t)
 '(vc-handled-backends ( ))
 '(warning-suppress-types '((comp)))
 '(wdired-allow-to-change-permissions t))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(eshell-prompt ((t (:foreground "MediumPurple2" :weight bold))))
 '(slime-repl-input-face ((t (:foreground "#2de0a7" :weight bold))))
 '(slime-repl-result-face ((t (:foreground "#ccccff"))))
 '(variable-pitch ((t (:inherit default :height 1.0 :family "Fira Code")))))
