;;; -*- lexical-binding: t -*-
(defconst fifo--identifier '--fifo-- "The tagname for fifo queues. It is used by both `fifo' and `fifop'")

(defun fifo (&rest objects)
  "Return a newly created fifo queue with specified arguments as elements.
Any number of arguments, even zero arguments, are allowed."
  (let ((fifo (cons fifo--identifier nil))
	prev next)
    (when objects
      (setf (cdr fifo) (fifo-dll (pop objects))
	    prev (cdr fifo))
      (dolist (o objects)
	(setf next (fifo-dll o prev)
	      prev next))
      (fifo-dll-link prev (cdr fifo)))
    fifo))

(defun fifop (object)
  "Returns t if object is a fifo queue, otherwise returns nil"
  (and (consp object)
       (eq (car object) fifo--identifier)
       (or (not (cdr object))
	   (and (fifo-dll-p (cdr object))
		(eq (cdr object)
		    (fifo-dll-next (fifo-dll-prev (cdr object))))))))

(defun fifo-dll-p (object)
  "Returns t if object is a cons cell in the form (a . (b . c)), with \"b\" and \"c\" pointing to more cons cells, otherwise returns nil"
  (and (consp object)
       (consp (cdr object))
       (consp (cadr object))
       (consp (cddr object))))

(defun fifo-dll (object &optional prev next)
  "Returns a new cons cell in the form (OBJECT . (PREV . NEXT)) (a cyclic double linked list)

OBJECT is any lisp object.

Optional arguments PREV and NEXT must satisfy `fifo-dll-p' (ie, have the above structure).

If provided, they shall be set as previous and next nodes for the newly created list node, and their pointers modified to reference this node as next and previous node respectively

Otherwise, PREV and NEXT are this same node"
  (let ((c (cons object (cons prev next))))
    (if (fifo-dll-p prev)
	(fifo-dll-set-next prev c)
      (fifo-dll-set-prev c c))
    (if (fifo-dll-p next)
	(fifo-dll-set-prev next c)
      (fifo-dll-set-next c c))
    c))

(defmacro fifo-dll-next (dln)
  "Get DLN's next reference. DLN should satisfy `fifo-dll-p'"
  (declare (debug t))
  `(cdr-safe (cdr-safe ,dln)))

(defmacro fifo-dll-prev (dln)
  "Get DLN's previous reference. DLN should satisfy `fifo-dll-p'"
  (declare (debug t))
  `(car-safe (cdr-safe ,dln)))

(defmacro fifo-dll-set-next (current next)
  "Set CURRENT's next reference to NEXT. Both arguments should satisfy `fifo-dll-p'"
  (declare (debug t))  
  `(setf (cddr ,current) ,next))

(defmacro fifo-dll-set-prev (current prev)
  "Set CURRENT's previous reference to PREV. Both arguments should satisfy `fifo-dll-p'"
  (declare (debug t))
  `(setf (cadr ,current) ,prev))

(defmacro fifo-dll-link (prev next)
  "Link PREV and NEXT together, this is equivalent to using both `fifo-dll-set-prev' and `fifo-dll-set-next'"
  (declare (debug t))
  `(setf (cadr ,next) ,prev
	 (cddr ,prev) ,next))

(defmacro fifo-dll-unlink (dln)
  "Unlink dll node DLN from it's neighboring nodes"
  (declare (debug t))
  `(fifo-dll-link (fifo-dll-prev ,dln) (fifo-dll-next ,dln)))

(defun fifo-push (newelt fifo)
  "Add NEWELT into the fifo queue FIFO. returns FIFO"
  (if (fifop fifo)
      (let ((circle (cdr fifo)))
	(unless (fifo-dll-p newelt)
	  (setf newelt (fifo-dll newelt)))
	(if (null circle)
	    (setf (cdr fifo) newelt)
	  (fifo-dll-link (fifo-dll-prev circle) newelt)
	  (fifo-dll-link newelt circle))
	fifo)
    (error "Argument is not a fifo queue")))

(defun fifo-pop (fifo)
  "Dequeue and return the oldest element from the fifo queue FIFO"
  (if (fifop fifo)
      (let* ((circle (cdr fifo))
	     (item (car circle)))
	(if (eql circle (fifo-dll-next circle))
	    (setf (cdr fifo) nil)
	  (fifo-dll-unlink circle)
	  (setf (cdr fifo) (fifo-dll-next circle)))
	item)
    (error "Argument is not a fifo queue")))

(defun fifo-first (fifo)
  "Same as `fifo-pop', but don't remove (dequeue) the element"
  (if (and (fifop fifo)
	   (not (seq-empty-p fifo)))
      (cadr fifo)
    (error "Argument is not a fifo queue or queue is empty")))

(defun fifo-last (fifo)
  "The inverse of `fifo-first'"
  (if (and (fifop fifo)
	   (not (seq-empty-p fifo)))
      (car (fifo-dll-prev (cdr fifo)))
    (error "Argument is not a fifo queue or queue is empty")))

(defun fifo-flush (fifo)
  "Returns a list with all the elements from the fifo queue FIFO and dequeue them, otherwise return nil"
  (if (fifop fifo)
      (let (list)
	(while (not (seq-empty-p fifo))
	  (push (fifo-pop fifo) list))
	(nreverse list))
    (error "Argument is not a fifo queue")))

(defun fifo-dump (fifo seq)
  "Enqueue each element of sequence SEQ into fifo queue FIFO and return t if succesfull, nil otherwise"
  (if (and (fifop fifo)
	   (seqp seq)
	   (not (seq-empty-p seq)))
      (progn
	(seq-doseq (y seq)
	  (fifo-push y fifo))
	t)
    nil))

(cl-generic-define-generalizer fifo--generalizer
  12
  (lambda (name &rest args)
    `(when (fifop ,name)
       'fifo))
  (lambda (tag &rest args)
    (when (eq tag 'fifo)
      '(fifo cons list sequence))))

(cl-defmethod cl-generic-generalizers ((specializer (eql fifo)))
  "`fifo' queue generalizer - specializer for `fifo' data type"
  (list fifo--generalizer))

(cl-defmethod seqp ((fifo fifo))
  t)

(cl-defmethod seq-empty-p ((fifo fifo))
  "Returns t if there aren't any elements on the queue, nil otherwise"
  (not (cdr fifo)))

(cl-defmethod seq-length ((fifo fifo))
  "Returns the number of elements on the fifo queue FIFO "
  (let* ((circle (cdr fifo))
	 (next (fifo-dll-next circle))
	 (size 0))
    (when (not (seq-empty-p fifo))
      (setf size 1)
      (while (not (eql circle next))
	(setf size (1+ size)
	      next (fifo-dll-next next))))
    size))

(cl-defmethod seq-elt ((fifo fifo) number)
  "Returns the element at index NUMBER from the fifo queue FIFO

Negative indexes counts from the end of the queue, so (seq-elt (fifo 'A 'B 'C) -1) would yield C

With NUMBER a non integer, or out of range index, signal an error

With an empty queue, also signal an error

Notice that nil is a valid queue element, if an index returns it, that is the element found"
  (when (not (integerp number))
    (error "Wrong type argument"))

  (when (seq-empty-p fifo)
      (error "Queue is empty"))
  
  (let (index item current circle found)
    (setf circle (cdr fifo))
    (if (wholenump number)
	(progn
	  (setf index 0
		current circle)
	  (if (= index number)
	      (setf item (car current))
	    (setf index (1+ index)
		  current (fifo-dll-next circle))
	    (while (not found)
	      (cond ((and (eq current circle)
			  (not (= index number)))
		     (error "Index out of range"))
		    ((= index number)
		     (setf item (car current)
			   found t))
		    (t
		     (setf index (1+ index)
			   current (fifo-dll-next current)))))))
      (setf index -1
	    circle (fifo-dll-prev circle)
	    current circle)
      (if (= index number)
	  (setf item (car current))
	(setf index (1- index)
	      current (fifo-dll-prev circle))
	(while (not found)
	  (cond ((and (eq current circle)
		      (not (= index number)))
		 (error "Index out of range"))
		((= index number)
		 (setf item (car current)
		       found t))
		(t
		 (setf index (1- index)
		       current (fifo-dll-prev current)))))))
    item))

(cl-defmethod seq-do (func (fifo fifo))
  "Apply FUNCTION to each element on FIFO, in queue order

Used for side effects only"
  (let* ((circle (cdr fifo))
	 (current circle))
    (when circle
      (cl-flet ((func func))
	(func (car current))
	(setf current (fifo-dll-next circle))
	(while (not (eq current circle))
	  (func (car current))
	  (setf current (fifo-dll-next current)))))))

(cl-defmethod seq-subseq ((fifo fifo) start &optional end)
  "Returns a fifo queue with the elements in the range delimited by START (inclusive) and END (exclusive) from fifo queue FIFO

START must be an integer (ie, satisfy `integerp')

END, if omitted, set to t, or non integer, means include everything from START

With START or END equal or greater than the queue length, signal an error

If START > END and both are on the same polarity ( + - ), their values shall be swapped so that the oposite helds true

If (= START END), or if the fifo is empty, return an empty fifo

Negative indexes counts from the end of the queue rather than the start, so, for example, passing 2 as START and -3 as END would get everything between the third element on the queue (inclusive) and the third to last one (exclusive)

As fifo queues are cyclic structures, mixing negative and positive indexes may result in cyclic order. For example, (seq-subseq (fifo 'A 'B 'C 'D 'E 'F 'G 'H 'I) 3 -3) will result in a fifo with the queue ('D 'E 'F). But (seq-subseq (fifo 'A 'B 'C 'D 'E 'F 'G 'H 'I) -3 3) will result in a fifo with the queue ('G 'H 'I 'A 'B 'C)"
  (when (not (integerp start))
    (error "Start is not an Index"))

  (cond ((and end
	      (not (integerp end)))
	 (error "End is not an Index"))
	((and (seq-empty-p fifo)
	      (or (not (= start 0))
		  (and end
		       (not (= end 0)))))
	 (error "Attempt to get a slice ( %s %s ) out of an empty queue" start end))
	((and (not (integerp end))
	      (null end))
	 (setf end t)))

  (when (and (not (eq end t))
	     (> start end)
	     (eq (wholenump start)
		 (wholenump end)))
    (let ((int start))
      (setf start end
	    end int)))
  
  (let* ((collection (fifo))
	 (circle (cdr fifo))
	 (index (if (wholenump start) 0 -1))
	 (current circle)
	 found)
    (when (not (or (seq-empty-p fifo)
		   (and (integerp end)
			(= start end))))
      (if (wholenump start)
	  (if (= start index)
	      (setf start circle)
	    (setf index (1+ index)
		  current (fifo-dll-next circle))
	    (while (not found)
	      (cond ((= start index)
		     (setf start current
			   found t)
		     (when (eq start circle)
		       (setf start (fifo-dll-prev circle))))
		    ((eq current circle)
		     (error "Start out of range"))
		    (t
		     (setf index (1+ index)
			   current (fifo-dll-next current))))))
	(let ((circle (fifo-dll-prev circle)))
	  (if (= start index)
	      (setf start circle)
	    (setf index (1- index)
		  current (fifo-dll-prev circle))
	    (while (not found)
	      (cond ((= start index)
		     (setf start current
			   found t)
		     (when (eq start circle)
		       (setf start (fifo-dll-next circle))))
		    ((eq current circle)
		     (error "Start out of range"))
		    (t
		     (setf index (1- index)
			   current (fifo-dll-prev current))))))))
      (when (and end
		 (not (eq t end)))
	(setf index (if (wholenump end) 0 -1)
	      found nil)
	(if (wholenump end)
	    (if (= end index)
		(setf end circle)
	      (setf index (1+ index)
		    current (fifo-dll-next circle))
	      (while (not found)
		(cond ((= end index)
		       (setf end current
			     found t)
		       (when (eq end circle)
		       	 (setf end t)))
		      ((eq current circle)
		       (error "End out of range"))
		      (t
		       (setf index (1+ index)
			     current (fifo-dll-next current))))))
	  (let ((circle (fifo-dll-prev circle)))
	    (if (= end index)
		(setf end circle)
	      (setf index (1- index)
		    current (fifo-dll-prev circle))
	      (while (not found)
		(cond ((= end index)
		       (setf end current
			     found t)
		       (when (eq end circle)
		       	 (setf end (fifo-dll-next circle))))
		      ((eq current circle)
		       (error "End out of range"))
		      (t
		       (setf index (1- index)
			     current (fifo-dll-prev current)))))))))
      (unless (or (eq start end)
		  (and (eq end t)
		       (eq start (fifo-dll-prev circle))))
	(when (eq end t)
	  (setf end circle))
	(fifo-push (car start) collection)
	(setf current (fifo-dll-next start))
	(while (not (eq current end))
	  (fifo-push (car current) collection)
	  (setf current (fifo-dll-next current)))))
    collection))

(cl-defmethod seq-into-sequence ((fifo fifo))
  "Returns a fifo queue as a list, or nil if the queue is empty

Similar to `fifo-flush', but doesn't dequeue the elements"
  (let (list)
    (seq-doseq (y fifo)
      (push y list))
    (nreverse list)))

(cl-defmethod seq-into ((fifo fifo) type)
  "Similar to `seq-into-sequence', but rather than a list, the resulting sequence is determined by TYPE, where TYPE is one of three symbols: vector, string and list"
  (pcase type
    ('list (seq-into-sequence fifo))
    ('vector (vconcat (seq-into-sequence fifo)))
    ('string (let (collection)
	       (dolist (y (seq-into-sequence  fifo) (apply #'concat (nreverse collection)))
		 (push (format "%s" y) collection))))
    (_ (error "Only list, vector or string symbol is accepted as TYPE argument"))))

(cl-defmethod seq-copy ((fifo fifo))
  "Returns a new fifo queue with contents identical to FIFO"
  (let ((collection (fifo)))
    (when (not (seq-empty-p fifo))
      (seq-doseq (y fifo)
	(fifo-push y collection)))
    collection))

(cl-defmethod seq-drop ((fifo fifo) n)
  "Returns a new fifo queue equal to FIFO minus the first N elements

With N out of < 1 - (1- (seq-length fifo)) > range, return a full copy"
  (when (not (integerp n))
    (error "Not an integer"))
  
  (seq-subseq fifo (1+ (min (max 0 n) (1- (seq-length fifo))))))

(cl-defmethod seq-take ((fifo fifo) n)
  "Copy the first N elements of the fifo queue FIFO onto a new fifo queue, and returns it

with N out of < 1 - (1- (seq-length fifo)) > range, return the new fifo empty"
  (seq-subseq fifo 0 (min (max n 0) (seq-length fifo))))
