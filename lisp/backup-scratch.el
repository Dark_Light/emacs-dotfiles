(progn (cleanup)) (progn (cleanup))

(loop as pattern in (list "~/.config/gdfuse/*" "~/.config/common/lisp/quicklisp")
      as entry in (eshell-extended-glob (or pattern "~/"))
      collecting entry)

(aio-with-async
  (let* ((xdg (or (getenv "XDG_CONFIG_HOME") (expand-file-name ".config" user-home-dir)))
	 (xdg-inclusions '("emacs" "common-lisp" "enchive" "fanficfare" "gdfuse" "git" "guix"
			   "htop" "IPFS" "mcomix" "MegaFuse" "mupen64plus" "nyxt" "password-store"
			   "stumpwm" "systemd" "tmux" "transmission-daemon" "x11" "zsh"))
	 (xdg-exclusions '("emacs/straight/" "gdfuse/*/cache/" "common/lisp/quicklisp/" "guix/current"))
	 (bup-dir (expand-file-name "dark" xdg))
	 (exclusions-file (make-temp-file "bup-exclusions-"))
	 sources exclusions)
    (dolist (y xdg-inclusions) (push (expand-file-name y xdg) sources))
    (loop as pattern in (xdg-exclusions)
	  do (loop as entry in (eshell-extended-glob (expand-file-name pattern xdg))
		   do (push entry exclusions)))
    (loop as pattern in (list "gdfuse/*" "common/lisp/quicklisp/")
	  as entry in (eshell-extended-glob pattern)
	  collecting entry)
    (with-temp-file exclusions-file
      (dolist (entry exclusions) (insert entry) (newline)))
    (unless (file-directory-p bup-dir)
      (cl-destructuring-bind (sentinel . promise) (aio-make-callback :once t)
	(make-process :name "bup init"
		      :command (list "bup" "init")
		      :sentinel sentinel)
	(aio-await promise)))
    (cl-destructuring-bind (sentinel . promise) (aio-make-callback :once t)
      (make-process :name "bup index"
		    :command `("bup" "--bup-dir" ,bup-dir "index" "--update"
			       "--exclude-from" ,exclusions-file ,@sources)
		    :sentinel sentinel)
      (aio-await promise))
    (cl-destructuring-bind (sentinel . promise) (aio-make-callback :once t)
      (make-process :name "bup save"
		    :command `("bup" "--bup-dir" ,bup-dir "save"
			       "--name" ,(concat (user-real-login-name) "-in-" (system-name)) ,@sources)
		    :sentinel sentinel)
      (aio-await promise))
    (when (file-exists-p exclusions-file)
      (delete-file exclusions-file))))

(aio-with-async
  (let* ((xdg (file-name-as-directory (or (getenv "XDG_CONFIG_HOME")
					  (expand-file-name ".config" user-home-directory))))
	 (xdg-inclusions '("emacs" "common-lisp" "enchive" "fanficfare" "gdfuse" "git" "guix"
			   "htop" "IPFS" "mcomix" "MegaFuse" "mupen64plus" "nyxt" "password-store"
			   "stumpwm" "systemd" "tmux" "transmission-daemon" "x11" "zsh"))
	 (xdg-exclusions '("emacs/straight" "gdfuse/*/cache" "common/lisp/quicklisp" "guix/current"))
	 (bup-dir (file-name-as-directory (expand-file-name "bup/dark" xdg)))
	 (exclusions-file (make-temp-file "bup-exclusions-"))
	 sources exclusions)
    (dolist (y xdg-inclusions) (push (file-name-as-directory (expand-file-name y xdg)) sources))
    (loop as pattern in (xdg-exclusions)
	  do (loop as entry in (eshell-extended-glob (expand-file-name pattern xdg))
		   do (push entry exclusions)))
    (loop as pattern in (list "gdfuse/*" "common/lisp/quicklisp/")
	  as entry in (eshell-extended-glob pattern)
	  collecting entry)
    (with-temp-file exclusions-file
      (dolist (entry exclusions) (insert entry) (newline)))
    (unless (file-directory-p bup-dir)
      (cl-destructuring-bind (sentinel . promise) (aio-make-callback :once t)
	(make-process :name "bup init"
		      :command (list "bup" "init")
		      :sentinel sentinel)
	(aio-await promise)))
    (cl-destructuring-bind (sentinel . promise) (aio-make-callback :once t)
      (make-process :name "bup index"
		    :command `("bup" "--bup-dir" ,bup-dir "index" "--update"
			       "--exclude-from" ,exclusions-file ,@sources)
		    :sentinel sentinel)
      (aio-await promise))
    (cl-destructuring-bind (sentinel . promise) (aio-make-callback :once t)
      (make-process :name "bup save"
		    :command `("bup" "--bup-dir" ,bup-dir "save"
			       "--name" ,(concat (user-real-login-name) "-in-" (system-name)) ,@sources)
		    :sentinel sentinel)
      (aio-await promise))
    (when (file-exists-p exclusions-file)
      (delete-file exclusions-file))))

(aio-with-async
  (let* ((xdg (file-name-as-directory (or (getenv "XDG_CONFIG_HOME")
					  (expand-file-name ".config" user-home-directory))))
	 (xdg-inclusions '("emacs" "common-lisp" "enchive" "fanficfare" "gdfuse" "git" "guix"
			   "htop" "IPFS" "mcomix" "MegaFuse" "mupen64plus" "nyxt" "password-store"
			   "stumpwm" "systemd" "tmux" "transmission-daemon" "x11" "zsh"))
	 (xdg-exclusions '("emacs/straight" "gdfuse/*/cache" "common/lisp/quicklisp" "guix/current"))
	 (bup-dir (file-name-as-directory (expand-file-name "bup/dark" xdg)))
	 (exclusions-file (make-temp-file "bup-exclusions-"))
	 sources exclusions)
    (cl-flet ((cleanup ( ) (when (file-exists-p exclusions-file) (delete-file exclusions-file))))
      (condition-case error
	  (progn (with-temp-file exclusions-file
		   (dolist (entry exclusions) (insert entry) (newline)))
		 (dolist (y xdg-inclusions) (push (file-name-as-directory (expand-file-name y xdg)) sources))
		 (loop as pattern in (xdg-exclusions)
		       do (loop as entry in (eshell-extended-glob (expand-file-name pattern xdg))
				do (push entry exclusions)))
		 (loop as pattern in (list "~/.config/gdfuse/*" "~/.config/common/lisp/quicklisp")
		       as entry in (eshell-extended-glob (or pattern "~/"))
		       collecting entry)
		 (unless (file-directory-p bup-dir)
		   (cl-destructuring-bind (sentinel . promise) (aio-make-callback :once t)
		     (make-process :name "bup init"
				   :command (list "bup" "init")
				   :sentinel sentinel)
		     (aio-await promise)))
		 (cl-destructuring-bind (sentinel . promise) (aio-make-callback :once t)
		   (make-process :name "bup index"
				 :command `("bup" "--bup-dir" ,bup-dir "index" "--update"
					    "--exclude-from" ,exclusions-file ,@sources)
				 :sentinel sentinel)
		   (aio-await promise))
		 (cl-destructuring-bind (sentinel . promise) (aio-make-callback :once t)
		   (make-process :name "bup save"
				 :command `("bup" "--bup-dir" ,bup-dir "save"
					    "--name" ,(concat (user-real-login-name) "-in-" (system-name)) ,@sources)
				 :sentinel sentinel)
		   (aio-await promise))
		 (cleanup))
	(cleanup)))))

(cl-flet ((cleanup ( ) (when (file-exists-p exclusions-file) (delete-file exclusions-file)))))

(aio-await (bup-ensure-repo-exists))

(cl-destructuring-bind (sentinel . promise) (aio-make-callback :once t)
    (make-process :name "bup save"
		  :command `("bup" "--bup-dir" ,bup-dir "save"
			     "--name" ,(concat (user-real-login-name) "-in-" (system-name)) ,@bup-inclusions)
		  :sentinel sentinel)
    (aio-await promise))
