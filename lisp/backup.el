;; -*- lexical-binding: t -*-
(eval-when-compile (require 'aio))

;; Dynamic variables don't work with these async funcions

;; (defvar bup-sources ( ) "List of paths to index, save, and restore")
;; (defvar bup-exclusions ( ) "List of paths to exclude from trees on `bup-sources'")
;; (defvar bup-dir "" "Location of bup repository")

(aio-defun bup-task (name command &optional stderr)
  "Abstraction of `make-process' that returns the exit status code

(fn name command &optional stderr)"
  (cl-destructuring-bind (sentinel . promise) (aio-make-callback :once t)
    (let ((process (apply 'make-process (append (list :name name :command command :sentinel sentinel)
						(when stderr (list :stderr stderr))))))
      (aio-await promise) (process-exit-status process))))

(aio-defun bup-ensure-repo-exists (repository)
  "Ensure the bup git repository pointed to by REPOSITORY exists, and it creates it if necesary

When REPOSITORY is anything other than a git repository, remove and create it anew

(fn repository)"
  ;; Use git to tell whether repository is a git repo
  (unless (zerop (aio-await (bup-task "git rev-parse" (list "git" "-C" repository "rev-parse"))))
    ;; When repository exists but is anything other than a directory
    ;; simply delete it first
    (when (file-exists-p repository) (delete-file repository))
    ;; Wait for "bup init" to do it's magic, and return t or nil based on success
    (zerop (aio-await (bup-task "bup init" (list "bup" "--bup-dir" repository "init"))))))

(defmacro bup--do-op (op repository sources args)
  "Perform bup operation OP, with ARGS, on an asynchronous process, and warn about any error that occurs, returning a boolean that indicates success

DYNAMIC-ARGS are supplied at run time while STATIC-ARGS are passed at macro expansion time and are put before

The bup operation OP, backup repository REPOSITORY and sources from SOURCES are handled specially, other args can be freely passed by way of ARGS

Non zero status codes are interpreted as errors with the contents of stderr as the message, resulting in a warning with said message as contents

This macro should only be used in an async context such as `aio-with-async'"
  (declare (indent defun))
  (let ((error-message (gensym))
	(error-output (gensym))
	(boolean (gensym)))
    `(let ((,error-output (generate-new-buffer "*bup-temp-buffer* "))
	   ,error-message)
       (aio-await (bup-ensure-repo-exists ,repository))
       (if (zerop (prog1 (aio-await (bup-task ,(format "bup %s" op)
					      (append (list "bup" "--bup-dir" ,repository ,op)
						      ,args ,sources)
					      ,error-output))
		    (setf ,error-message (with-current-buffer ,error-output (buffer-string)))
		    (when (buffer-live-p ,error-output) (kill-buffer ,error-output))))
	   t
	 (prog1 ( )
	   (warn ,(format "[bup] %s failed: \n\n %%s" op) ,error-message))))))

(aio-defun bup-index (repository sources &optional exclusions &rest args)
  "Index entries based on the values of `bup-sources' and `bup-exclusions'

--update option is implicitly passed to bup

--exclude-from option is implicitly passed to bup when EXCLUSIONS is non nil, and handled especially

(fn repository sources &optional exclusions &rest args)"
  (let ((exclusions-file (when exclusions (make-temp-file "bup-exclusions-file-"))))
    (when exclusions-file
      (with-temp-file exclusions-file
	(cl-loop as entry in exclusions do (insert entry) (newline)))
      (setf args (append (list "--exclude-from" exclusions-file) args)))
    (setf args (append (list "--update") args))
    (prog1 (bup--do-op "index" repository sources args)
      (when (and exclusions-file (file-exists-p exclusions-file)) (delete-file exclusions-file)))))

(aio-defun bup-save (repository sources &rest args)
  "Save entries previously indexed

(fn repository sources &rest args)"
  (bup--do-op "save" repository sources args))

(aio-defun bup-restore (repository sources &rest args)
  "Restore entries previously saved

(fn repository sources &rest args)"
  (bup--do-op "restore" repository sources args))

(aio-defun bup-backup-home ( )
  "Backup the home directory

(fn)"
  (let* ((xdg (file-name-as-directory (or (getenv "XDG_CONFIG_HOME")
					  (expand-file-name ".config" user-home-directory))))
	 (xdg-inclusions '("emacs" "common-lisp" "enchive" "fanficfare" "gdfuse" "git" "guix"
			   "htop" "IPFS" "mcomix" "MegaFuse" "mupen64plus" "nyxt" "password-store"
			   "stumpwm" "systemd" "tmux" "transmission-daemon" "x11" "zsh" "git"))
	 (xdg-exclusions '("emacs/straight" "gdfuse/*/cache" "common-lisp/quicklisp" "guix/current"))
	 (bup-sources (cl-loop as y in xdg-inclusions
			       collecting (file-name-as-directory (expand-file-name y xdg))))
	 (bup-exclusions ( ))
	 (bup-dir (file-name-as-directory (expand-file-name "bup/dark" xdg))))
    (cl-loop as pattern in xdg-exclusions
	     do (setf bup-exclusions (append bup-exclusions
					     (cl-loop as entry in (eshell-extended-glob (expand-file-name pattern xdg))
						      collecting entry))))
    (aio-await (bup-index bup-dir bup-sources bup-exclusions))
    (aio-await (bup-save bup-dir bup-sources "--name" (format "%s-in-%s" (user-real-login-name) (system-name))))))

(aio-defun bup-restore-home ( )
  "Restore the home directory

(fn)"
  (let ((bup-dir (file-name-as-directory
		  (expand-file-name
		   "y"
		   (expand-file-name
		    "bup"
		    (or (getenv "XDG_CONFIG_HOME")
			(expand-file-name ".config" user-home-directory))))))
	(bup-sources ( )))
    (aio-await (bup-restore bup-dir bup-sources
			    "--outdir" user-home-directory
			    "--name" (format "%s-in-%s" (user-real-login-name) (system-name))))))
