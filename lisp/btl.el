;;; btl.el -- utilidades de btl -*- lexical-binding: t; -*-

(require 'transient)
(require 'eieio)
(require 'tramp)

(defvar btl-directory (expand-file-name "storage/shared/BTL/" user-home-directory)
  "Location for BTL stuff")

(define-inline btl--expand-file-name (name)
  (inline-letevals (name)
    (inline-quote (expand-file-name ,name btl-directory))))

(cl-defmacro btl--with-pdc-contents ((pdc &optional save) &body body)
  (declare (indent defun))
  (let* ((place `(btl--expand-file-name (format "%s.org" ,pdc)))
	 (run `((save-excursion (insert-file-contents-literally ,place))
		(re-search-forward (rx "*"))
		(org-mode)
		,@body)))
    `(condition-case ( )
	 ,(if save
	      `(let ((tramp-verbose 0))
		 (unless (file-exists-p ,place)
		   (btl--make-pdc-file ,place))
		 (with-temp-file ,place ,@run))
	    `(with-temp-buffer ,@run))
       (file-error (display-warning 'btl (format "unable to access pdc %s" ,pdc) :error ,place)))))

(defclass nestle-item ( )
  ((name :initform "" :initarg :name)
   (percentage :initform 0 :initarg :percentage)
   (faces :initform 0 :initarg :faces)
   (total-faces :initform 0 :initarg :total-faces)
   (expiry :initform "" :initarg :expiry)
   (shelf-stock :initform 0 :initarg :shelf-stock)
   (depot-stock :initform 0 :initarg :depot-stock)
   (present :initform t :initarg :present))
  "Base class for nestle items")

(defvar btl--pdc-properties (make-hash-table :size 6 :rehash-size 3)
  "Table mapping pdc properties to keywords")

(cl-loop as (key . property) in '((:entry . "ENTRADA")
				  (:exit . "SALIDA")
				  (:item . "ITEM")
				  (:faces . "CARAS")
				  (:expiry . "EXPIRA")
				  (:stock . "INVENTARIO"))
	 do (puthash key property btl--pdc-properties))

(cl-defun btl--pdc-get (pdc key)
  (cl-labels ((get (property) (org-entry-get (point) (gethash property btl--pdc-properties)))
	      (explode (property) (mapcar #'string-to-number (split-string (get property) "/"))))
    (btl--with-pdc-contents (pdc)
      (cl-case key
	(:entry (get :entry))
	(:exit (get :exit))
	(:items
	 (org-map-entries
	  (lambda ( )
	    (let ((item (intern (get :item))))
	      (cons item (let* ((total-faces (explode :faces))
				(faces (pop total-faces))
				(depot-stock (explode :stock))
				(shelf-stock (pop depot-stock)))
			   (make-instance 'nestle-item :name item
					  :faces faces
					  :total-faces (or (car-safe total-faces) faces)
					  :percentage (round (/ (* faces 100.0) (or (car-safe total-faces) faces)))
					  :expiry (get :expiry)
					  :shelf-stock shelf-stock
					  :depot-stock (or (car-safe depot-stock) 0))))))
	  "item&LEVEL=3" 'tree))
	(:notes (org-map-entries (lambda ( ) (get :item)) "nota&LEVEL=3" 'tree))))))

(defun btl--time->string (time &optional split)
  (let ((time-string (format-time-string "%d/%m/%Y %I:%M %p" (encode-time time))))
    (if split (split-string time-string (rx space) t) time-string)))

(defun btl--string->time (string)
  (cl-flet ((expand-year (year)
			 (let ((current-millenium (* (round (decoded-time-year (decode-time)) 1000) 1000)))
			   (or (and (< (ceiling (log year 10)) 4)
				    (+ current-millenium year))
			       year))))
    (let ((time (apply #'make-decoded-time (let ((current-time (decode-time)))
					     `(:dst ,(decoded-time-dst current-time)
						    :zone ,(decoded-time-zone current-time))))))
      (pcase (split-string string (rx space) t)
	((and `(,date ,hourly ,side)
	      (let `((,day ,month ,year) (,hour ,minute . ,second))
		(list (mapcar #'string-to-number (split-string date (rx "/") t))
		      (mapcar #'string-to-number (split-string hourly (rx ":") t)))))
	 (setf (decoded-time-second time) (or (car second) 0)
	       (decoded-time-minute time) minute
	       (decoded-time-hour time) (or (and (string-match-p side (rx (or "am" "AM")))
						 (mod hour 12))
					    (and (string-match-p side (rx (or "pm" "PM")))
						 (= hour 12)
						 hour)
					    (+ hour 12))
	       (decoded-time-day time) day
	       (decoded-time-month time) month
	       (decoded-time-year time) (expand-year year)))
	((and `(,date)
	      (let (or `(,day ,month ,year)
		       `(,month ,year))
		(mapcar #'string-to-number (split-string date (rx "/") t))))
	 (setf (decoded-time-second time) 0
	       (decoded-time-minute time) 0
	       (decoded-time-hour time) 0
	       (decoded-time-day time) (or day 1)
	       (decoded-time-month time) month
	       (decoded-time-year time) (expand-year year)))
	((and `(,date)
	      (let (or `(,hour)
		       `(,hour ,minute)
		       `(,hour ,minute ,second))
		(mapcar #'string-to-number (split-string date (rx ":") t))))
	 (let ((current-time (decode-time)))
	   (setf (decoded-time-second time) (or second 0)
		 (decoded-time-minute time) (or minute 0)
		 (decoded-time-hour time) hour
		 (decoded-time-day time) (decoded-time-day current-time)
		 (decoded-time-month time) (decoded-time-month current-time)
		 (decoded-time-year time) (decoded-time-year current-time)))))
      (decode-time (encode-time time)))))

(defun btl--pdc-save-time (pdc slot time)
  (btl--with-pdc-contents (pdc t)
    (org-entry-put (point) (gethash slot btl--pdc-properties) (btl--time->string time))))

(cl-defun btl--normalize-time (pdc &key (side :entry) time format)
  (let* ((dynamic-time (decode-time))
	 (final-time (cond ((stringp time) (let ((lisp-time (btl--string->time time)))
					     (btl--pdc-save-time pdc side lisp-time) lisp-time))
			   ((null time) (let ((saved-time (btl--pdc-get pdc side)))
					  (or (and saved-time (btl--string->time saved-time)) dynamic-time)))
			   (t (prog1 dynamic-time
				(btl--pdc-save-time pdc side dynamic-time))))))
    (or (and format
	     (split-string (btl--time->string final-time) (rx space) t))
	final-time)))

(defvar btl--items (make-hash-table :size 30 :rehash-size 3)
  "A table of nestle items")

(cl-loop as (entry title percentage units) in '((cubitos "Caldos Deshidratados (Cubitos)" 80 40)
						(sopas "Sopas y Cremas" 80 72)
						(sazonadores "Anaquel de Sazonadores" 20 52)
						(bases "Bases para pastas" 50 30)
						(maggi "Bloque Maggi" 80 181)
						(pure "Puré de Papas" 35 60)
						(nestea "Bebidas de Té en Polvo" 50 37)
						(nescafe "Cafe Instantáneo (Nescafé)" 60 15)
						(formulas "*Fórmulas Infantiles*" 70 24)
						(nestum "*Cereales Infantiles*" 90 24)
						(compotas "Colados / Compotas" 30 24)
						(camprolac "Leches de Crecimiento (Camprolac Forticrece)" 75 12)
						(nido "Leches de Crecimiento (Nido Forticrece)" 75 12)
						(campesina "Leche en Polvo Campesina" 40 12)
						(svelty "" 40 24)
						(cerelac "Cereales (Cerelac)" 100 18)
						(bai "Bloque de Alimentación Infantil (Bloque BAI)" 100 20)
						(milo "Bebida Achocolatada (Milo)" 20 24)
						(chichas "Chichas en Polvo (Rica Chicha)" 30 24)
						(leches "Leches (Campesina)" 40 18)
						(culinarias "Leches Culinarias" 100 48)
						(condensada "Leche Condensada" 70 48)
						(cremas "Cremas de Leche" 80 48)
						(moca "" 25 48)
						(carnation "" 60 48)
						(postres "Anaquel Repostería/Postres" 100 36)
						(chocolates "Anaquel de Chocolates" 70 91)
						(chocolates-gondola "Chocolates / Punta de Góndola" 100 91)
						(dulces-checkout "Chocolates / Wafer /Cubiertos- Check Out Supermercados" 70 91)
						(importados "Chocolates Importados (Prestigio/Kit Kat)" 40 24)
						(grageas "Chocolates Grageas (Toronto)" 40 36)
						(galletas "Anaquel Galletas/Wafer" 100 39)
						(gatos "*ALIMENTO PARA GATOS:*" 80 6)
						(perros "*ALIMENTO PARA PERROS:*" 45 6))
	 do (puthash entry `((:title . ,title) (:percentage . ,percentage) (:units-per-box . ,units))
		     btl--items))

(defvar btl--digital-kpi-items '(cubitos sopas sazonadores bases nestea
					 nescafe formulas nestum compotas
					 nido cerelac chichas leches culinarias
					 postres chocolates importados grageas galletas gatos perros)
  "List of items of the digital KPI")

(defvar btl--physical-kpi-items '(cubitos sopas sazonadores bases nestea nescafe
					  formulas nestum compotas nido cerelac
					  leches chichas culinarias chocolates galletas perros gatos)
  "List of items of the physical KPI")

(defvar btl--abstract-items ( ) ;; '((leches . (campesina svelty))
			        ;;   (culinarias . (condensada cremas moca carnation))
			        ;;   (maggi . (cubitos sopas sazonadores)))
  "Alist of items that are actually aggregates of other items")

(defvar btl--pdc-template-header "#+STARTUP: show2levels"
  "Header to insert at the very beginning of a pdc file")

(defvar btl--pdc-template-stock-properties `(("ITEM_ALL" . ,(mapconcat #'symbol-name (hash-table-keys btl--items) " "))
					     ("COLUMNS" . "%ITEM %CARAS(caras) %EXPIRA(expira) %INVENTARIO(inventario)"))
  "Properties to set on the stock item")

(defun btl--make-pdc-file (place &optional generate-items)
  "Generate a pdc file as described by PLACE, taking the basename as pdc and directory for location"
  (let ((default-directory (file-name-directory place))
	(name (file-name-base place)))
    (cl-flet ((generate-headline (entry &optional advance)
				 (if advance (org-insert-heading)
				   (org-insert-heading-after-current))
				 (org-edit-headline entry)))
      
      (with-temp-file (format "%s.org" name)
	(unless (derived-mode-p 'org-mode) (org-mode))
	(insert btl--pdc-template-header)
	(org-next-visible-heading 1)
	(generate-headline (capitalize name) t)
	(generate-headline "Inventario")
	(org-set-tags "item")
	(org-demote)
	(cl-loop as (property . entry) in btl--pdc-template-stock-properties
		 do (org-set-property property entry))
	(when generate-items
	  (cl-loop as entry in (hash-table-keys btl--items)
		   do (generate-headline (symbol-name entry))
		   initially (org-demote)
		   finally (org-up-heading-or-point-min) (org-goto-first-child) (org-promote)))
	(generate-headline "Observaciones")
	(org-set-tags "observacion")))))

(define-inline btl--item-get (item key)
  (inline-letevals (item key)
    (inline-quote (alist-get ,key (gethash ,item btl--items)))))

(defvar btl-boss "Ilset Sánchez" "The boss")
(defvar btl-route 8 "Current route")

(defun btl--generate-digital-kpi-header (pdc)
  (insert "📍BTL INFORMA🚩")
  (newline)
  (insert "CANAL MOD/FAR")
  (newline)
  (cl-destructuring-bind (date time side) (btl--normalize-time pdc :side :entry :format t)
    (insert (format "*Fecha:* %s" date))
    (newline)
    (insert "*Ciudad:* Caracas.")
    (newline)
    (insert (format "*Ruta:* %d." btl-route))
    (newline)
    (insert (format "*PDV:* %s" pdc))
    (newline)
    (insert (format "*Promotor:* Lineker Ramirez."))
    (newline)
    (insert (format "*Hora de visita:* %s %s" time side)))
  (newline))

(defun btl--generate-digital-kpi-section ( )
  (cl-loop repeat 39
	   as y = t then (not y)
	   do (insert (if y "-" " "))
	   finally (newline)))

(cl-defun btl--generate-digital-kpi-item (item)
  (with-slots (name faces total-faces expiry percentage depot-stock present) item
    (insert (format "%s%s" (btl--item-get name :title) (if present "" ": *no hay*")))
    (newline)
    (insert (format "*S.O.S establecido %s%% (Porcentaje de participación en anaquel)*"
		    (btl--item-get name :percentage)))
    (newline)
    (insert (format "- Caras Nestlé: %s" (if present faces "")))
    (newline)
    (insert (format "- Caras Totales: %s" (if present total-faces "")))
    (newline)
    (insert (format "- Indique S.O.S: %s" (if present (format "%s%%" percentage) "")))
    (newline)
    (insert (format "- Fecha de Vencimiento más próxima: %s" (if present expiry "")))
    (newline)
    (insert (format "- Número de Cajas en depósito: %s" (if present depot-stock "")))
    (newline 2)))

(defvar btl--digital-kpi-categories (make-hash-table :size 9 :rehash-size 3)
  "A table of KPI categories, each encompasing one or more items")

(cl-loop as entry in '((culinarios "*CULINARIOS*" 1 cubitos sopas sazonadores)
		       (bebidas "*BEBIDAS*" 1 nestea)
		       (nutricion "*NUTRICIÓN*" 1 formulas nestum compotas)
		       (lacteos "*LACTEOS*" 1 leches)
		       (confites "*CONFITES*" 1 chocolates importados)
		       (galletas "Galletas" 1 galletas)
		       (animales "" 0 gatos perros))
	 do (puthash (car entry) `((:title . ,(cadr entry))
				   (:start-index . ,(caddr entry))
				   (:items . ,(cdddr entry)))
		     btl--digital-kpi-categories))

(define-inline btl--digital-kpi-category-get (category key)
  (inline-letevals (category key)
    (inline-quote (alist-get ,key (gethash ,category btl--digital-kpi-categories)))))

(defun btl--generate-digital-kpi-category (category data)
  (cl-loop as entry in (btl--digital-kpi-category-get category :items)
	   as item = (or (alist-get entry data) (make-instance 'nestle-item :name entry :present ( )))
	   with index = (btl--digital-kpi-category-get category :start-index)
	   initially (let ((title (btl--digital-kpi-category-get category :title)))
		       (unless (string-empty-p title)
			 (insert (format "%s" title))
			 (newline)))
	   unless (zerop index)
	   do (insert (format "%d) " index))
	   (cl-incf index)
	   do (btl--generate-digital-kpi-item item)))

(defun btl--generate-digital-kpi (pdc)
  (cl-loop with items-data = (btl--pdc-get pdc :items)
	   as name in '(culinarios bebidas nutricion lacteos confites galletas animales)
	   as entry = (gethash name btl--digital-kpi-categories)
	   initially (btl--generate-digital-kpi-header pdc)
	   (btl--generate-digital-kpi-section)
	   do (btl--generate-digital-kpi-category name items-data))
  (cl-loop as entry in (btl--pdc-get pdc :notes)
	   initially
	   (delete-backward-char 1)
	   (btl--generate-digital-kpi-section)
	   (newline)
	   (insert "Observaciones:")
	   (newline)
	   do (insert entry) (newline 2)
	   finally return t))

(defmacro btl--with-temp-file (basename &rest body)
  (declare (indent defun))
  `(with-temp-file (btl--expand-file-name (format "%s.txt" ,basename))
     ,@body))

(defun btl--share-content ( )
  (let ((status (call-process-region ( ) ( ) "termux-share" ( ) ( ) ( ) "-a" "send" "-c" "text/plain")))
    (and (numberp status)
	 (zerop status)
	 t)))

(defun btl-write-kpi (pdc)
  (interactive "s PDC: ")
  (btl--with-temp-file pdc
    (btl--generate-digital-kpi pdc)))

(defun btl-share-kpi (pdc)
  (interactive "s PDC: ")
  (with-temp-buffer
    (btl--generate-digital-kpi pdc)
    (btl--share-content)))

(cl-defun btl--generate-point (pdc &key side time)
  (let* ((time (btl--normalize-time pdc :side side :time time))
	 (hour (decoded-time-hour time)))
    (insert (cond 
	     ((and (>= hour 6) (< hour 12)) "BUENOS DÍAS.")
	     ((and (>= hour 12) (< hour 19)) "BUENAS TARDES.")
	     (t "BUENAS NOCHES.")))
    (newline)
    (insert "📍 *PUNTO ACTIVO BTL* 📍")
    (newline)
    (insert (format "PDC: %s" pdc))
    (newline)
    (insert (format "Supervisor: %s" btl-boss))
    (newline)
    (insert "Colaborador: Lineker Ramirez")
    (newline)
    (cl-destructuring-bind (date time dayside) (btl--time->string time t)
      (insert (format "Día/fecha: %s" date))
      (newline)
      (insert (format "Hora de %s: %s %s" (if (eq side :entry) "llegada" "salida") time dayside))))
  (newline)
  (insert (format "Ruta: %d" btl-route))
  (newline)
  (insert "Cargo: Promotor")
  (newline)
  t)

(defun btl-write-active-point (pdc &optional time)
  (interactive (list (read-string "PDC: ") current-prefix-arg))
  (btl--with-temp-file (format "Punto Activo %s" pdc)
    (btl--generate-point pdc :side :entry :time time)))

(defun btl-write-exit-point (pdc &optional time)
  (interactive (list (read-string "PDC: ") current-prefix-arg))
  (btl--with-temp-file (format "Punto Salida %s" pdc)
    (btl--generate-point pdc :side :exit :time time)))

(defun btl-share-active-point (pdc &optional time)
  (interactive (list (read-string "PDC: ") current-prefix-arg))
  (with-temp-buffer
    (btl--generate-point pdc :side :entry :time time)
    (btl--share-content)))

(defun btl-share-exit-point (pdc &optional time)
  (interactive (list (read-string "PDC: ") current-prefix-arg))
  (with-temp-buffer
    (btl--generate-point pdc :side :exit :time time)
    (btl--share-content)))

(defun btl-most-recent-expiry (pdc)
  "Get the most recent expiry date from PDC"
  (cl-loop as (name . item) in (btl--pdc-get "Gama Plus La Trinidad" :items)
	   as date = (encode-time (btl--string->time (slot-value item :expiry)))
	   as minimal = date then (if (time-less-p minimal date) minimal date)
	   finally return (decode-time minimal)))

(defvar btl--physical-kpi-properties `(("ITEM_ALL" . ,(mapconcat (lambda (entry)
								   (symbol-name (or (and (symbolp entry)
											 entry)
										    (car entry))))
								 btl--physical-kpi-items
								 " "))
				       ("COLUMNS" . ,(cl-loop as entry in '("anaquel" "deposito" "participacion")
							      collecting (format "%%%s(%s)" (upcase entry) entry)
							      into content
							      finally return (string-join (cons "%ITEM" content) " ")))))

(defun btl-generate-physical-kpi (pdc)
  (with-current-buffer (get-buffer-create (format "*%s Physical KPI*" pdc))
    (org-columns-quit)
    (erase-buffer)
    (unless (derived-mode-p 'org-mode) (org-mode))
    (org-insert-heading)
    (org-edit-headline "Root")
    (cl-loop as (property . entry) in btl--physical-kpi-properties
	     do (org-set-property property entry))
    (cl-loop as entry in btl--physical-kpi-items
	     as abstract = (alist-get entry btl--abstract-items)
	     with items = (btl--pdc-get pdc :items)
	     as item = (if abstract
			   (cl-loop as (item-name . item) in items
				    with components = abstract
				    with abstract = (make-instance 'nestle-item :name entry)
				    with units-per-box = 0
				    with participants = 0
				    do (when (member item-name components)
					 (cl-incf (slot-value abstract :shelf-stock) (slot-value item :shelf-stock))
					 (cl-incf (slot-value abstract :depot-stock) (slot-value item :depot-stock))
					 (cl-incf (slot-value abstract :faces) (slot-value item :faces))
					 (cl-incf (slot-value abstract :total-faces) (slot-value item :total-faces))
					 (cl-incf participants)
					 (cl-incf units-per-box (btl--item-get (slot-value item :name)
									       :units-per-box)))
				    finally do (if (zerop participants) (setf (slot-value abstract :present) ( ))
						 (with-slots (faces total-faces shelf-stock percentage) abstract
						   (setf percentage (/ (* faces 100.0) total-faces)
							 shelf-stock (ceiling (/ shelf-stock
										 (/ (float units-per-box)
										    (float participants)))))))
				    (cl-return abstract))
			 (or (alist-get entry items)
			     (make-instance 'nestle-item :name entry)))
	     when item do (org-insert-heading-after-current)
	     (org-edit-headline (symbol-name entry))
	     (unless abstract (with-slots (shelf-stock name) item
				(setf shelf-stock (ceiling shelf-stock (btl--item-get name :units-per-box)))))
	     (org-set-property "ANAQUEL" (format "%d" (slot-value item :shelf-stock)))
	     (org-set-property "DEPOSITO" (format "%d" (slot-value item :depot-stock)))
	     (org-set-property "PARTICIPACION" (format "%d" (slot-value item :percentage)))
	     initially (org-demote) (require 'org-colview)
	     finally (org-up-heading-all 1)
	     (org-promote)
	     (org-columns)
	     (org-columns-content)
	     (cl-return (current-buffer)))))

(defvar btl-image-directory (expand-file-name (file-name-concat "storage" "dcim" "Nestle") user-home-directory)
  "Directory where btl related images are placed")

(defvar btl-image-regex (rx "TimePhoto" "_" (+ digit) "_" (+ digit) "." "jpg")
  "Regex that matches images in `btl-image-directory'")

(defvar btl-image-stale-threesold (* 3600 24 7)
  "Amount of seconds after which an image in `btl-image-directory' becomes stale and eligible for garbage collection")

(defun btl-garbage-collect-images ( )
  "Clean up stale images in `btl-image-directory' that are beyond `btl-image-stale-threesold' old"
  (cl-loop as entry in (directory-files btl-image-directory t btl-image-regex t)
	   as age = (time-to-seconds (file-attribute-modification-time (file-attributes entry)))
	   with now = (time-to-seconds)
	   when (> (- now age) btl-image-stale-threesold)
	   do (delete-file entry)
	   and collect entry))

(transient-define-prefix btl ( ) "Write and Share Entry, Exit and KPIs"
  ["Write"
   [("k" "Kpi" btl-write-kpi)
    ("a" "Active Point" btl-write-active-point)
    ("e" "Exit Point" btl-write-exit-point)]]
  ["Share"
   [("K" "Kpi" btl-share-kpi)
    ("A" "Active Point" btl-share-active-point)
    ("E" "Exit Point" btl-share-exit-point)]])

(provide 'btl)
