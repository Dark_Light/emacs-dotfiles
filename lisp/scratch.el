;;; scratch.el --- Persistence for things of the *scratch* buffer -*- lexical-binding: t -*-

(list :alicorn :pony :goddess)

(defvar berries '("Strawberry" "Blueberry" "Blackberry" "Raspberry" "Salmonberry" "Cranberry" "Gooseberry" "Huckleberry" "Saskatoonberry"))

(defvar labs '("cats.elc" "aqua.el" "sky.el" "void.el" "energy.el" "matter.el" "." ".." "testing.e" "calm" "peace" "tranquil" "rainbow.el" "boxes.rc"))

(defconst Goddess (intern (symbol-name 'Goddess)) "Self Evaluating Symbol")

(defvar possible-eshell-prompt-regexp
  "*\s[0-9a-zA-Z]+\s@\s[0-9a-zA-Z]+\sin\s.*\\(?:on\sgit:[0-9a-zAZ]+\s\\(?:o\\|x\\)\\)?\\(?:\s(\s\d\s)\\)?\n★"
  "A possible regexp which should match a string like: * dark @ mooncat in ~ on git:master x ( 0 )
★")

(defmacro ney-async-do (&rest body)
  "Run body forms in a child emacs process, with both init file and mlp loaded and ready

Returns an aio promise that eventually resolves to the value of the last form or an error"
  (let ((promise (gensym 'mlp))
	(mlp-lib-path (file-name-sans-extension (or (locate-file (concat "mlp.elc") load-path)
						    (locate-file (concat "mlp.el") load-path))))
	(aio-lib-path (file-name-sans-extension (or (locate-file (concat "aio.elc") load-path)
						    (locate-file (concat "aio.el") load-path))))
	(async-lib-path (file-name-sans-extension (or (locate-file (concat "async.elc") load-path)
						      (locate-file (concat "async.el") load-path)))))
    `(let ((,promise (aio-promise)))
       (prog1 ,promise
	 (async-start
	  (lambda ( )
	    (condition-case error
		(list :success
		      (progn
			(load (expand-file-name "init" user-emacs-directory))
			(cl-loop as (lib . path) in `((mlp . ,,mlp-lib-path)
						      (aio . ,,aio-lib-path)
						      (async . ,,async-lib-path))
				 do (load path)
				 (require lib))
			,@body))
	      (error (push :error error))))
	  (lambda (result)
	    (aio-resolve ,promise (lambda ( )
				    (if (eq :success (car result))
					(cadr result)
				      (signal (cadr result) (cddr result)))))))))))

(setf ney (ney-async-do (fboundp 'aio-make-callback)))

(funcall (aio-result ney))

`(cons mlp "goddess")

(with-temp-buffer
  (insert-file-contents "/home/dark/Media/Libros/MLP/Surviving Saiyan in Equestria.html")
  (let ((content (libxml-parse-html-region (point-min) (point-max))))
    (with-current-buffer "Goldy"
      (setf (buffer-string) (format "%S" content)))))

(defun initial-buffer-choice ( )
  "Set initial buffer"
  (if (fboundp 'slime)
      (progn
	(when (not (fboundp 'slime-scratch))
	  (slime-setup))
	(slime-scratch-buffer))
    (let ((* (get-buffer-create "*scratch*")))
      (with-current-buffer *
	(lisp-interaction-mode))
      *)))

;; Directory Variables
(dir-locals-set-class-variables 'shell-config-directory
				'((nil . ((mode . shell-script)))))

(dir-locals-set-directory-class (file-name-as-directory (getenv "SHELL_CONFIG_DIR")) 'shell-config-directory)

(let ((collection '(smartparens slime skewer-mode multi-term macrostep dired+ magit aio)))
  (cl-loop as descriptor in init-packages
	   when (and (symbolp descriptor)
		     (memq descriptor collection))
	   do (require descriptor)
	   else
	   do (when (and (consp descriptor)
			 (memq (car descriptor) collection))
		(require (car descriptor)))))

;; if this is my dev emacsen, switch to the right init file
  (when (> (length (split-string emacs-version "\\.")) 2)
    (let ((user-emacs-directory (expand-file-name "emacs-dev/"
						  (or (getenv "XDG_CONFIG_HOME")
						      (expand-file-name ".config" (getenv "HOME"))))))
      (load (expand-file-name "init" user-emacs-directory) t t))
    (cl-return-from :init))

(defun init-login ( )
  "Start or connect to a daemon named after `user-real-login-name' and use it as a login shell

This function should only be called within a batch emacs instance running as a login shell within unix"
  (require 'server)
  (let ((address (format "--daemon=%s" (user-real-login-name))))
    (unless (server-running-p (user-real-login-name))
      (call-process (car command-line-args) nil 0 nil address))
    (call-process "xinit" nil 0 nil)))

(defun system-processor-cores ( )
  "Return the number of processor cores in the system"
  (cl-ecase system-type
    (gnu/linux (when (file-exists-p "/proc/cpuinfo")
		 (with-temp-buffer
		   (insert-file-contents-literally "/proc/cpuinfo")
		   (how-many "^processor[[:space:]]+:"))))
    (windows-nt (when-let ((processors (getenv "NUMBER_OF_PROCESSORS")))
		  (string-to-number processors)))
    (gnu/kfreebsd (with-temp-buffer
		    (ignore-errors
		      (when (zerop (call-process "sysctl" nil t nil "-n" "hw.ncpu"))
			(string-to-number (buffer-string))))))))
