;; -*- lexical-binding: t; -*-
(require 'aio)

(aio-defun download-music (&rest args)
  "Download the music from LIST using youtube-dl as a child process

DIRECTORY tells where to download the url, the rest of arguments are relative to this by default

LIST provides the urls to download, but any url already present in SUCCESS-LOG is skipped

While LIST is assumed to be a list of urls, it can also be a filename with one such url per line.

SUCCESS-LOG and FAIL-LOG are always files, with SUCCESS-LOG being read in order to get already successful entries

SUCCESS-LOG and FAIL-LOG are also written to in order to log succesful and failed downloads

When retry is non nil, keep trying downloading failed entries from FAIL-LOG

Return the list of succesfully downloaded entries

\(fn &key (directory user-home-directory) (list (expand-file-name \"music\") directory) (success-log (expand-file-name \"downloaded\" directory)) (fail-log (expand-file-name \"failed\" directory)))"
  (cl-destructuring-bind (&key retry (directory user-home-directory)
			       (list (expand-file-name "music" directory))
			       (success-log (expand-file-name "downloaded" directory))
			       (fail-log (expand-file-name "failed" directory)))
      args
    (let ((save-silently t)
	  (default-directory directory)
	  (success-list (file-to-list success-log))
	  (command '("yt-dlp" "--quiet" "--format=bestaudio" "--extract-audio" "--add-metadata")))
      (cl-loop as entry in (cl-etypecase list
			     (list list)
			     (string (file-to-list list)))
	       with command = `(,@command "--retries=10" "--fragment-retries=10")
	       do (unless (member entry success-list)
		    (pcase-let* ((`(,sentinel . ,promise) (aio-make-callback :once t))
				 (arguments `(:name ,(car command) :command ,`(,@command ,entry)
						    :sentinel ,sentinel :connection-type pipe))
				 (status (progn (apply #'make-process arguments)
						(process-exit-status (car (aio-await promise)))))
				 (success (and (numberp status) (zerop status))))
		      (when success (push entry success-list))
		      (append-to-file (format "%s\n" entry) ( ) (if success success-log fail-log)))))
      (cl-loop with to-download = (file-to-list fail-log)
	       as entry in (copy-sequence to-download)
	       with command = `(,@command "--retries=infinite" "--fragment-retries=infinite")
	       initially (unless retry (cl-return))
	       do (cl-loop as (sentinel . promise) = (aio-make-callback :once t)
			   as arguments = `(:name ,(car command) :command ,`(,@command ,entry)
						  :sentinel ,sentinel :connection-type pipe)
			   as status = (progn (apply #'make-process arguments)
					      (process-exit-status (car (aio-await promise))))
			   until (and (numberp status) (zerop status))
			   finally (push entry success-list)
			   (setf to-download (remq entry to-download))
			   (append-to-file (format "%s\n" entry) ( ) success-log)
			   (with-temp-file fail-log (insert (string-join to-download "\n")))))
      success-list)))

;; (cl-defun y (x &key y) y)

;; (let* ((context (expand-file-name "Musica Maury" user-home-directory))
;;        (list (with-temp-buffer
;; 	       (cl-loop initially (save-excursion
;; 				    (insert-file-contents-literally (expand-file-name "music" context)))
;; 			as entry = (nth 4 (split-string (thing-at-point 'line t)))
;; 			until (eobp) collect entry and do (forward-line)))))
;;   (setf goldy (download-music :directory context :list list :retry t)))

(aio-defun transfer-music (&rest args)
  "Transfer music to DESTINATION from DIRECTORY in local host. Destination may be any tramp filename

\(fn destination &key retry (directory (expand-file-name \"Media/Musica\" user-home-directory)) (sucess-log (expand-file-name \"transferred\" directory)) (fail-log (expand-file-name \"grounded\" directory)))"
  (cl-destructuring-bind (destination &key retry
				      (directory (expand-file-name "Media/Musica" user-home-directory))
				      (sucess-log (expand-file-name "transferred" directory))
				      (fail-log (expand-file-name "grounded" directory)))
      args
    (let ((save-silently t)
	  (default-directory directory)
	  (sucess-list (file-to-list sucess-log (rx (* space) "\n" (* space))))
	  (arguments '(:name "scp" :connection-type pipe)))
      (cl-loop as entry in (directory-files directory ( ) (rx (+ anything) "." (or "opus" "m4a")))
	       unless (member entry sucess-list)
	       do (pcase-let* ((`(,sentinel . ,promise) (aio-make-callback :once t))
			       (status (progn (apply #'make-process
						     `(,@arguments :sentinel ,sentinel
								   :command ("scp" ,entry ,destination)))
					      (process-exit-status (car (aio-await promise)))))
			       (sucess (and (numberp status) (zerop status))))
		    (when sucess (push entry sucess-list))
		    (append-to-file (format "%s\n" entry) ( ) (if sucess sucess-log fail-log))))
      (cl-loop with to-transfer = (file-to-list fail-log (rx (* space) "\n" (* space)))
	       as entry in (copy-sequence to-transfer)
	       initially (unless retry (cl-return))
	       do (cl-loop as (sentinel . promise) = (aio-make-callback :once t)
			   as status = (progn (apply #'make-process
						     `(,@arguments :sentinel ,sentinel
								   :command ("scp" ,entry ,destination)))
					      (process-exit-status (car (aio-await promise))))
			   until (and (numberp status) (zerop status))
			   finally (push entry sucess-list)
			   (setf to-transfer (remq entry to-transfer))
			   (append-to-file (format "%s\n" entry) ( ) sucess-log)
			   (with-temp-file fail-log (insert (string-join to-transfer "\n")))))
      sucess-list)))

;; (setf goldy (transfer-music "neocat:storage/music" :directory "~/Musica Maury" :retry t))
