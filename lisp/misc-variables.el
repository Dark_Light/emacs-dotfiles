;;; -*- lexical-binding: t -*-
;;Aliases
(defalias 'repl 'ielm)

(setq berries '("Strawberry" "Blueberry" "Blackberry" "Raspberry" "Salmonberry" "Cranberry" "Gooseberry" "Huckleberry" "Saskatoonberry"))

(setq labs '("cats.elc" "aqua.el" "sky.el" "void.el" "energy.el" "matter.el" "." ".." "testing.e" "calm" "peace" "tranquil" "rainbow.el" "boxes.rc"))

(setf base "@o°·*")

(setf packages '(tiny stream smartparens slime skewer-mode simple-httpd rjsx-mode rebecca-theme multi-term macrostep js2-mode emms anzu dash))

(defconst Goddess (intern (symbol-name 'Goddess)) "Self Evaluating Constant")
