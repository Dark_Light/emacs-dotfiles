;;; -*- lexical-binding: t -*-
(cl-defstruct (fifo (:constructor fifo--create)
		    (:predicate fifop)
		    (:copier nil))
  queue length)

(cl-defun fifo-create (&rest objects)
  "Return a newly created fifo queue with specified arguments as elements.
Any number of arguments, even zero arguments, are allowed."
  (let ((fifo (fifo--create :length 0)) prev next length)
    (when objects
      (setf (fifo-queue fifo) (fifo-dln-create (pop objects))
	    prev (fifo-queue fifo)
	    length 1)
      (dolist (y objects)
	(setf next (fifo-dln-create y prev)
	      prev next
	      length (1+ length)))
      (fifo-dln-link prev (fifo-queue fifo))
      (setf (fifo-length fifo) length))
    fifo))

(cl-defstruct (fifo-dln (:constructor fifo-dln--create)
			(:copier nil))
  value prev next)

(cl-defun fifo-dln-create (object &optional prev next)
  "Returns a cyclic double linked node

OBJECT is any lisp object.

Optional arguments PREV and NEXT must satisfy `fifo-dln-p'.

If provided, they shall be set as previous and next nodes for the newly created list node, and their pointers modified to reference this node as next and previous node respectively

Otherwise, PREV and NEXT are this same node"
  (let ((node (fifo-dln--create :value object)))
    (if (fifo-dln-p prev)
	(fifo-dln-link prev node)
      (setf (fifo-dln-prev node) node))
    (if (fifo-dln-p next)
	(fifo-dln-link node next)
      (setf (fifo-dln-next node) node))
    node))

(cl-defmacro fifo-dln-link (prev next)
  "Link PREV and NEXT together"
  (declare (debug t))
  `(setf (fifo-dln-prev ,next) ,prev
	 (fifo-dln-next ,prev) ,next))

(cl-defmacro fifo-dln-unlink (dln)
  "Unlink dln node DLN from it's neighboring nodes"
  (declare (debug t))
  `(fifo-dln-link (fifo-dln-prev ,dln) (fifo-dln-next ,dln)))

(cl-defun fifo-push (newelt fifo)
  "Add NEWELT into the fifo queue FIFO. returns FIFO"
  (if (fifop fifo)
      (let ((circle (fifo-queue fifo))
	    (length (fifo-length fifo)))
	(unless (fifo-dln-p newelt)
	  (setf newelt (fifo-dln-create newelt)))
	(cond ((= length 0)
	       (setf (fifo-queue fifo) newelt
		     (fifo-length fifo) 1))
	      ((> length 0)
	       (fifo-dln-link (fifo-dln-prev circle) newelt)
	       (fifo-dln-link newelt circle)
	       (setf (fifo-length fifo) (1+ length))))
	fifo)
    (error "Argument is not a fifo queue")))

(cl-defun fifo-pop (fifo)
  "Dequeue and return the oldest element from the fifo queue FIFO"
  (if (fifop fifo)
      (let* ((circle (fifo-queue fifo))
	     (item (and circle (fifo-dln-value circle)))
	     (length (fifo-length fifo)))
	(cond ((= length 1)
	       (setf (fifo-queue fifo) nil
		     (fifo-length fifo) 0))
	      ((> length 1)
	       (fifo-dln-unlink circle)
	       (setf (fifo-queue fifo) (fifo-dln-next circle)
		     (fifo-length fifo) (1- length)))
	      (t
	       (error "Empty Queue")))
	item)
    (error "Argument is not a fifo queue")))

(cl-defun fifo-first (fifo)
  "Same as `fifo-pop', but don't remove (dequeue) the element"
  (if (and (fifop fifo)
	   (not (seq-empty-p fifo)))
      (fifo-dln-value (fifo-queue fifo))
    (error "Argument is not a fifo queue or queue is empty")))

(cl-defun fifo-last (fifo)
  "The inverse of `fifo-first'"
  (if (and (fifop fifo)
	   (not (seq-empty-p fifo)))
      (fifo-dln-value (fifo-dln-prev (fifo-queue fifo)))
    (error "Argument is not a fifo queue or queue is empty")))

(cl-defun fifo-flush (fifo)
  "Returns a list with all the elements from the fifo queue FIFO and dequeue them, otherwise return nil"
  (if (fifop fifo)
      (let (list)
	(while (not (seq-empty-p fifo))
	  (push (fifo-pop fifo) list))
	(nreverse list))
    (error "Argument is not a fifo queue")))

(cl-defun fifo-dump (fifo seq)
  "Enqueue each element of sequence SEQ into fifo queue FIFO and return t if succesfull, nil otherwise"
  (if (and (fifop fifo)
	   (seqp seq)
	   (not (seq-empty-p seq)))
      (progn
	(seq-doseq (y seq)
	  (fifo-push y fifo))
	t)
    nil))

(cl-defmethod seqp ((_ fifo))
  "Implementation of `seqp' for fifo types. Always returns t"
  t)

(cl-defmethod seq-empty-p ((_ fifo))
  "Returns t if there aren't any elements on the queue, nil otherwise"
  (not (fifo-queue _)))

(cl-defmethod seq-length ((_ fifo))
  "Returns the number of elements on the fifo queue FIFO "
  (fifo-length _))

(cl-defmethod seq-elt ((queue fifo) number)
  "Returns the element at index NUMBER from the fifo queue FIFO

Negative indexes counts from the end of the queue, so (seq-elt (fifo 'A 'B 'C) -1) would yield C

With NUMBER a non integer, or out of range index, signal an error

With an empty queue, also signal an error

Notice that nil is a valid queue element, if an index returns it, that is the element found"
  (cond ((not (integerp number))
	 (error "Wrong type argument"))

	((seq-empty-p queue)
	 (error "Queue is empty"))
	
	(t
	 (let (index item current circle (length (fifo-length queue)))
	   (setf circle (fifo-queue queue))
	   (if (wholenump number)
	       (if (> number (1- length))
		   (error "Index out of range: %s" number)
		 (setf index 0
		       current circle)
		 (if (= index number)
		     (setf item (fifo-dln-value current))
		   (setf current (fifo-dln-next current)
			 index (1+ index))
		   (while (not (> index number))
		     (when (= index number)
		       (setf item (fifo-dln-value current)))
		     (setf index (1+ index)
			   current (fifo-dln-next current)))))
	     (setf index -1
		   circle (fifo-dln-prev circle)
		   current circle)
	     (if (> (abs number) length)
		 (error "Index out of range: %s" number)
	       (if (= index number)
		   (setf item (fifo-dln-value current))
		 (setf current (fifo-dln-prev current)
		       index (1- index))
		 (while (not (< index number))
		   (when (= index number)
		     (setf item (fifo-dln-value current)))
		   (setf index (1- index)
			 current (fifo-dln-prev current))))))
	   item))))

(cl-defmethod seq-do (func (fifo fifo))
  "Apply FUNCTION to each element on FIFO, in queue order

Used for side effects only"
  (let* ((circle (fifo-queue fifo))
	 (current circle))
    (when circle
      (cl-flet ((func func))
	(func (fifo-dln-value current))
	(setf current (fifo-dln-next circle))
	(while (not (eq current circle))
	  (func (fifo-dln-value current))
	  (setf current (fifo-dln-next current)))))))

(cl-defmethod seq-subseq ((queue fifo) start &optional end)
  "Returns a fifo queue with the elements in the range delimited by START (inclusive) and END (exclusive) from fifo queue FIFO

START must be an integer (ie, satisfy `integerp')

END, if omitted, set to t, or non integer, means include everything from START

With START or END equal or greater than the queue length, signal an error

If START > END and both are on the same polarity ( + - ), their values shall be swapped so that the oposite helds true

If (= START END), or if the fifo is empty, return an empty fifo

Negative indexes counts from the end of the queue rather than the start, so, for example, passing 2 as START and -3 as END would get everything between the third element on the queue (inclusive) and the third to last one (exclusive)

As fifo queues are cyclic structures, mixing negative and positive indexes may result in cyclic order. For example, (seq-subseq (fifo 'A 'B 'C 'D 'E 'F 'G 'H 'I) 3 -3) will result in a fifo with the queue ('D 'E 'F). But (seq-subseq (fifo 'A 'B 'C 'D 'E 'F 'G 'H 'I) -3 3) will result in a fifo with the queue ('G 'H 'I 'A 'B 'C)"
  (cond ((not (integerp start))
	 (signal 'wrong-type-argument (list 'integerp start)))
	((and end
	      (not (integerp end)))
	 (signal 'wrong-type-argument (list 'integerp end)))
	((and (seq-empty-p queue)
	      (or (not (= start 0))
		  (and end
		       (not (= end 0)))))
	 (error "Attempt to get a slice ( %s %s ) out of an empty queue" start end)))

  (when (and end
	     (> start end)
	     (eq (wholenump start)
		 (wholenump end)))
    (let ((int start))
      (setf start end
	    end int)))
  
  (let* ((collection (fifo-create))
	 (circle (fifo-queue queue))
	 (index (if (wholenump start) 0 -1))
	 (current circle)
	 (length (fifo-length queue))
	 found)
    (when (not (or (seq-empty-p queue)
		   (and end
			(= start end))
		   (= start length)))
      
      (when (or (> (abs start) length)
		(and end
		     (> (abs end) length)))
	(error "Invalid Range: ( %s %s )" start (or end length)))
      
      (if (wholenump start)
	  (if (= start index)
	      (setf start circle)
	    (setf index (1+ index)
		  current (fifo-dln-next circle))
	    (while (not found)
	      (if (= start index)
		  (setf start current
			found t)
		(setf index (1+ index)
		      current (fifo-dln-next current)))))
	(let ((circle (fifo-dln-prev circle)))
	  (if (= start index)
	      (setf start circle)
	    (setf index (1- index)
		  current (fifo-dln-prev circle))
	    (while (not found)
	      (if (= start index)
		  (setf start current
			found t)
		(setf index (1- index)
		      current (fifo-dln-prev current)))))))
      (when end
	(setf index (if (wholenump end) 0 -1)
	      found nil)
	(if (wholenump end)
	    (if (= end index)
		(setf end circle)
	      (setf index (1+ index)
		    current (fifo-dln-next circle))
	      (while (not found)
		(if (= end index)
		    (setf end current
			  found t)
		  (setf index (1+ index)
			current (fifo-dln-next current)))))
	  (let ((circle (fifo-dln-prev circle)))
	    (if (= end index)
		(setf end circle)
	      (setf index (1- index)
		    current (fifo-dln-prev circle))
	      (while (not found)
		(if (= end index)
		    (setf end current
			  found t)
		  (setf index (1- index)
			current (fifo-dln-prev current))))))))
      (unless (eq start end)
	(when (not end)
	  (setf end circle))
	(fifo-push (fifo-dln-value start) collection)
	(setf current (fifo-dln-next start))
	(while (not (eq current end))
	  (fifo-push (fifo-dln-value current) collection)
	  (setf current (fifo-dln-next current)))))
    collection))

(cl-defmethod seq-into-sequence ((queue fifo))
  "Returns a fifo queue as a list, or nil if the queue is empty

Similar to `fifo-flush', but doesn't dequeue the elements"
  (let (list)
    (seq-doseq (y fifo)
      (push y list))
    (nreverse list)))

(cl-defmethod seq-into ((fifo fifo) type)
  "Similar to `seq-into-sequence', but rather than a list, the resulting sequence is determined by TYPE, where TYPE is one of three symbols: vector, string and list"
  (pcase type
    ('list (seq-into-sequence fifo))
    ('vector (vconcat (seq-into-sequence fifo)))
    ('string (let (collection)
	       (dolist (y (seq-into-sequence  fifo) (apply #'concat (nreverse collection)))
		 (push (format "%s" y) collection))))
    (_ (error "Only list, vector or string symbol is accepted as TYPE argument"))))

(cl-defmethod seq-copy ((fifo fifo))
  "Returns a new fifo queue with contents identical to FIFO"
  (let ((collection (fifo-create)))
    (when (not (seq-empty-p fifo))
      (seq-doseq (y fifo)
	(fifo-push y collection)))
    collection))
