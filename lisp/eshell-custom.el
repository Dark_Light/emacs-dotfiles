(defun batch-eshell-source-file ( )
  "Same as `eshell-source-file', except that it only works in batch mode, and takes an arbitrary number of arguments after the file to source. SUBCOMMAND-P is not used

Both the file and the arguments comes from the remaining command line arguments. Afterwards kill emacs with status code 0 if no error ocurred during sourcing, 1 otherwise"
  (defvar command-line-args-left)
  (when (and noninteractive command-line-args-left)
    (setf attempt-stack-overflow-recovery nil
	  attempt-orderly-shutdown-on-fatal-signal nil
	  eval-expression-debug-on-error nil)
    (let ((eshell-non-interactive-p t)
	  (file (pop command-line-args-left)))
      (if (and (file-regular-p file)
	       (file-readable-p file))
	  (with-temp-buffer
	    (eshell-mode)
	    (eshell-do-eval `(eshell-commands (list (eshell-source-file ,file (quote ,command-line-args-left)))))
	    (kill-emacs eshell-last-command-status))
	(kill-emacs 1)))))

(defun batch-eshell-source-files ( )
  "Same as `batch-eshell-source-file', except that it sources an arbitrary number of files with no arguments"
  (defvar command-line-args-left)
  (when (and noninteractive command-line-args-left)
    (setf attempt-stack-overflow-recovery nil
	  attempt-orderly-shutdown-on-fatal-signal nil
	  eval-expression-debug-on-error nil)
    (with-temp-buffer
      (let ((eshell-non-interactive-p t))
	(eshell-mode)
	(cl-loop as file in command-line-args-left
		 do (ignore-errors (eshell-do-eval `(eshell-commands (list (eshell-source-file ,file))))))
	(kill-emacs eshell-last-command-status)))))

(defun eshell-brace-expand (&rest args)
  (cond
   ((null args) (list ""))
   ((null (cdr args))
    (let ((head (car args)))
      (cond
       ((vectorp head) (cl-mapcan #'eshell-brace-expand head))
       ((listp head) (apply #'eshell-brace-expand head))
       ((atom head) (list (format "%s" head))))))
   (t (let ((eshell-brace-expand-tail (eshell-brace-expand (cdr args)))
            (r (list)))
        (dolist (x (eshell-brace-expand (car args)))
          (dolist (y eshell-brace-expand-tail)
            (push (concat x y) r)))
        (reverse r)))))
