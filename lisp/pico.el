;;; pico.el --- Picolisp IDE -*- lexical-binding: t; -*-

;; Version: 0
;; URL: https://github.com/Dark_Light/Pico
;; Package-Requires: ((emacs "28.1.90"))
;; Keywords: languages, lisp, pico

;;; commentary

;; A picolisp interaction library. Unlike other already existing such
;; libraries at the time of it's inception, this one distinguishes
;; itself by being heavily inspired by sly.el. That means the
;; inclusion of features such as:

;; * Multiple repls (mrepls)
;; * backreferences
;; * multiple lisp processes, whith a default one to be used globally

;; Naturally, identation and font coloring is also performed on
;; code. This library is esentially the picolisp equivalent of sly.el

;;; code:

(eval-when-compile (require 'cl-lib))

(cl-eval-when (:compile-toplevel :load-toplevel))

(define-derived-mode pico-mode lisp-mode "Pico" "Major Mode for Editing Picolisp Code"
  (setq-local comment-start "#"))

;;; pico.el ends here
;; Local Variables:
;; coding: utf-8
;; End:
