;;; -*- lexical-binding: t -*-
(defconst canilla-ingredientes '((:harina . 150) (:agua . 85) (:levadura . 0.75) (:sal . 3) (:azucar . 6.25) (:grasa . 5)) "Lista asociativa con las cantidades de cada ingrediente requerido para hacer una canilla venezolana")

(defvar canilla-inventario '(:harina 150 :agua 85 :levadura 0.75 :sal 3 :azucar 6.25 :grasa 5) "Lista de prodiedades con las cantidades disponibles de cada ingrediente, en gramos")

(defun canilla (cantidad)
  "Notifica si se pueden producir CANTIDAD de canillas, y si de hecho sobra para mas, o si hace falta materia prima, cuanto de cada ingrediente, y cuantas canillas pueden hacerse actualmente, basado en la informacion de `canilla-inventario'"

  (when (not (wholenump cantidad))
    (user-error "La cantidad de canillas debe ser un natural: (wholenump %s)" cantidad))

  (let ((cantidades (canilla-stock))
	disponible stock)
    (pcase-dolist (`(,(pred keywordp) . ,(and (pred numberp) disponible)) cantidades)
      (push disponible stock))
    (setf disponible (apply min stock))
    (cond
     ((= 0 disponible)
      (canilla-sin-materia-prima))
     ((<= cantidad disponible)
      (canilla-hay-materia-prima))
     (t
      (canilla-insuficiente-materia-prima)))))

(defun canilla-stock ( )
  "Regresa una lista asociativa con el numero de canillas para las que cada ingrediente rinde segun la informacion disponible en `canilla-inventaro'"
  (let (cantidades)
    (pcase-dolist (`(,(and (pred keywordp) ingrediente) . ,(and (pred numberp) peso))
		   canilla-ingredientes)
      (push (cons ingrediente (floor (/ (plist-get canilla-inventario ingrediente) peso))) cantidades))
    cantidades))
