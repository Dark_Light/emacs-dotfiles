;;Make Any Address Clickable
(define-button-type 'find-file-button
  'follow-link t
  'action #'find-file-button)

(defun find-file-button (button)
  (find-file-read-only (buffer-substring (button-start button) (button-end button))))

(defun buttonize-buffer ()
  "Turn all file paths and URLs into buttons."
  (interactive)
  (require 'ffap)
  (deactivate-mark)
  (let (token guess beg end reached bound len)
    (save-excursion
      (goto-char (point-min))
      (setq reached (point))
      (while (re-search-forward ffap-next-regexp nil t)
	;; There seems to be a bug in ffap, Emacs 23.3.1: `ffap-file-at-point'
	;; enters endless loop when the string at point is "//".
	(setq token (ffap-string-at-point))
	(unless (string= "//" (substring token 0 2))
	  ;; Note that `ffap-next-regexp' only finds some "indicator string" for a
	  ;; file or URL. `ffap-string-at-point' blows this up into a token.
	  (save-excursion
	    (beginning-of-line)
	    (when (search-forward token (point-at-eol) t)
	      (setq beg (match-beginning 0)
		    end (match-end 0)
		    reached end))
	    )
	  (message "Found token <%s> at pos (%d-%d)" token beg (- end 1))
	  ;; Now let `ffap-guesser' identify the actual file path or URL at
	  ;; point.
	  (when (setq guess (ffap-guesser))
	    (message " Guessing %s" guess)
	    (save-excursion
	      (beginning-of-line)
	      (when (search-forward guess (point-at-eol) t)
		(setq len (length guess) end (point) beg (- end len))
		;; Finally we found something worth buttonizing. It shall have
		;; at least 2 chars, however.
		(message " Matched at pos (%d-%d)" beg (- end 1))
		(unless (or (< (length guess) 2))
		  (message " Buttonize!")
		  (make-button beg end :type 'find-file-button))
		)
	      )
	    )
	  ;; Continue one character after the guess, or the original token.
	  (goto-char (max reached end))
	  (message "Continuing at %d" (point))
	  )
	)
      )
    )
  )


(defun item-profit (price quantity &optional fee)
  "Get item's profit based on the price, quantity, and a fee, usually of 10%
All arguments must be numbers. Fee is interpreted as a percentage value"
  (when (or (not fee) (not (numberp fee)))
    (setf fee 10))
  (let* ((offert (* price quantity))
	 (tax (/ (* offert fee) 100))
	 (profit (- offert tax)))
    (round profit)))

;; (with-eval-after-load "simple-httpd"
;;   (progn
;;     (defservlet* fim/:history text/html ( )
;;       (cd httpd-root)
;;       (setf history (or history ".*"))
;;       (let (file)
;; 	(setf file (directory-files-recursively "ff" history))
;; 	(if (> (length file) 1)
;; 	    (let* ((length (length file))
;; 		   (random (random (1- length))))
;; 	      (setf file (nth random file)))
;; 	  (setf file (car file)))
;; 	(insert-file-contents file)))

;;     (defservlet* space/:file text/html ( )
;;       (cd (expand-file-name "Read" httpd-root))
;;       (let (target)
;; 	(setf target (cond ((and (not (null file))
;;     				 (file-exists-p file))
;;     			    file)
;;     			   ((and (not (null file))
;; 				 (file-exists-p (expand-file-name file httpd-root)))
;; 			    (expand-file-name file httpd-root))
;;     			   (t
;;     			    "read.html")))
;; 	(insert-file-contents target)))))

(defun fix-vlc-list ( )
  (interactive)
  (let* ((match "<\\(location\\)>\\(.*\\)</\\1>") (replace "<\\(info\\)>\\(.*\\)</\\1>\n") text (count 0))
    (save-excursion
      (goto-char (point-min))
      (while (re-search-forward match nil t)
	(save-excursion
	  (save-match-data
	    (when (re-search-forward replace nil t)
	      (setf text (match-string-no-properties 2))
	      (replace-match ""))))
	(when text
	  (replace-match text t nil nil 2)
	  (setf count (1+ count)
		text nil))))
    (when (>= count 1)
      (message "%d ocurrences" count))
    count))

(defun get-item-average (items)
  "<* Used for Reference in Toram Online *>
Get the average price of an item. Items should be an alist
whose elements have Quantity as Key and Price as Value

Returns the average unitary price of item"
  (let* ((length (length items))
	 (quantity 0)
	 (price quantity)
	 (acc price)
	 (avg acc))
    (dolist (item items acc)
      (setf quantity (car item)
	    price (cdr item)
	    acc (+ acc (/ price quantity))))
    (setf avg (/ acc length))
    (round avg)))
