;;; -*- lexical-binding: t -*-
(defun init-set-theme ( )
  "Set theme for non daemon emacs instances"
  (load-theme 'rebecca t))


(defun batch-eshell-source-file ( )
  "Same as `eshell-source-file', except that it only works in batch mode, and takes an arbitrary number of arguments after the file to source. SUBCOMMAND-P is not used

Both the file and the arguments comes from the remaining command line arguments. Afterwards kill emacs with status code 0 if no error ocurred during sourcing, 1 otherwise"
  (defvar command-line-args-left)
  (when (and noninteractive command-line-args-left)
    (setf attempt-stack-overflow-recovery nil
	  attempt-orderly-shutdown-on-fatal-signal nil)
    (let* (error result)
      (condition-case _
	  (setf result (eshell-command-result (format ". %s" (mapconcat #'identity command-line-args-left " "))))
	(error (setf error t)))
      (unless error
	(pp result))
      (kill-emacs (if error 1 0)))))

(defun batch-eshell-source-files ( )
  "Same as `batch-eshell-source-file', except that it sources an arbitrary number of files with no arguments")

(defun eshell-parse-brace-argument ( )
  "Parse a brace argument of the form ‘<exp>’, where exp can be:

* A comma separated list in the form regexp1,regexp2,regexp3...regexpn, each of which is a valid glob pattern subject to glob expansion, the end result being a list of either the regexes, or their expansion

Example: my-<n*,unique>/dir expands to (\"my-n*/dir\" \"my-unique/dir\") then globbing is applied and the final result may be something like (\"my-neat/dir\" \"my-nice/dir\" \"my-unique/dir\")

<n*,unique> would expand to (\"n*\" \"unique\") 

and if n* expands by globbing, then the final result would be something such as (\"neat\" \"nice\" \"unique\") 

Also note that this construct allows nesting: my-<functions,unique-<dir,place>> becomes (\"my-functions\" \"my-unique-dir\" \"my-unique-place\")

* An expression of the form integer1..integer2 where each integer represents a character results in an inclusive character range: <97..122> expands to (\"a\" \"b\" \"c\" ... \"z\"). which characters results depends of the coding systems in use

* An expression of the form integer1..integer2..integer3 where integer1 and integer2 are integers denoting an inclusive range. Integer3 determines the step

As an example: <0..10..3> would expand to (0 3 6 9), or every other 3rd number from 0 upto 10... which is the next number after the latest 3rd number, yet is also the last of the range, thus ending at 9

And <-9..10..1>, would be (-9 -8 -7 -6 -5 -4 -3 -2 -1 0 1 2 3 4 5 6 7 8 9 10)

Reverse ranges also works: <3..-3..1> expands to (3 2 1 0 -1 -2 -3)

And negative steps also reverses the ordering: <9..-3..3> expands to (9 6 0 -3) and <9..-3..-3> expands to (-3 0 3 6 9)

* If none of the above matches, <exp> assumes the {integer1..integer2} behavior, but with character classes as with [exp]

Example: <a-zA-Z> expands to (\"a\" \"b\" \"c\" ... \"z\" \"A\" \"B\" \"C\" ... \"Z\")

In all cases If there is text around and touching the braces then it is wrapped around each element of the list"
  ;; (when (and (not (eshell-arg-delimiter))
  ;; 	     (looking-at ".*<.*>")))
  )

; my-<functions,unique<dir,place,realm-<dimension,universe,megaverse,multiverse*>>>aey

(defun remove-nth-element (nth list)
  "Remove the nth element from a list non-destructively"
  (cond
   ((null list) list)
   ((zerop nth) (cdr list))
   (t (cons (car list)
            (remove-nth-element (- nth 1)
                                (cdr list))))))

;;Make Any Address Clickable
(define-button-type 'find-file-button
  'follow-link t
  'action #'find-file-button)

(defun find-file-button (button)
  (find-file-read-only (buffer-substring (button-start button) (button-end button))))

(defun buttonize-buffer ()
  "Turn all file paths and URLs into buttons."
  (interactive)
  (require 'ffap)
  (deactivate-mark)
  (let (token guess beg end reached bound len)
    (save-excursion
      (goto-char (point-min))
      (setq reached (point))
      (while (re-search-forward ffap-next-regexp nil t)
	;; There seems to be a bug in ffap, Emacs 23.3.1: `ffap-file-at-point'
	;; enters endless loop when the string at point is "//".
	(setq token (ffap-string-at-point))
	(unless (string= "//" (substring token 0 2))
	  ;; Note that `ffap-next-regexp' only finds some "indicator string" for a
	  ;; file or URL. `ffap-string-at-point' blows this up into a token.
	  (save-excursion
	    (beginning-of-line)
	    (when (search-forward token (point-at-eol) t)
	      (setq beg (match-beginning 0)
		    end (match-end 0)
		    reached end))
	    )
	  (message "Found token <%s> at pos (%d-%d)" token beg (- end 1))
	  ;; Now let `ffap-guesser' identify the actual file path or URL at
	  ;; point.
	  (when (setq guess (ffap-guesser))
	    (message " Guessing %s" guess)
	    (save-excursion
	      (beginning-of-line)
	      (when (search-forward guess (point-at-eol) t)
		(setq len (length guess) end (point) beg (- end len))
		;; Finally we found something worth buttonizing. It shall have
		;; at least 2 chars, however.
		(message " Matched at pos (%d-%d)" beg (- end 1))
		(unless (or (< (length guess) 2))
		  (message " Buttonize!")
		  (make-button beg end :type 'find-file-button))
		)
	      )
	    )
	  ;; Continue one character after the guess, or the original token.
	  (goto-char (max reached end))
	  (message "Continuing at %d" (point))
	  )
	)
      )
    )
  )

(defun xwidget-webkit-scroll-page-up ()
  "Scroll webkit up by one page. Page is the client height"
  (interactive)
  (xwidget-webkit-execute-script
   (xwidget-webkit-current-session)
   "window.scrollBy (0, ((document.body.clientHeight * 92) / 100));"))

(defun xwidget-webkit-scroll-page-down ()
  "Scroll webkit down by one page. Page is the client height"
  (interactive)
  (xwidget-webkit-execute-script
   (xwidget-webkit-current-session)
   "window.scrollBy (0, -((document.body.clientHeight * 92) / 100));"))

(defun xwidget-webkit-forward ()
  "Go forward in history."
  (interactive)
  (xwidget-webkit-execute-script (xwidget-webkit-current-session)
                                 "history.go (1);"))

(defun digits (X)
  "Return the number of decimal digits X have"
  (1+ (floor (log X 10))))

(defun get-item-average (items)
  "<* Used for Reference in Toram Online *>
Get the average price of an item. Items should be an alist
whose elements have Quantity as Key and Price as Value

Returns the average unitary price of item"
  (let* ((length (length items))
	 (quantity 0)
	 (price quantity)
	 (acc price)
	 (avg acc))
    (dolist (item items acc)
      (setf quantity (car item)
	    price (cdr item)
	    acc (+ acc (/ price quantity))))
    (setf avg (/ acc length))
    (round avg)))

(defun alist (&rest elements)
  "Build a new alist with the provided arguments in a Key Value order, as in a plist.
Returns the new alist"
  (let ((length (length elements))
        alist key value)
    (when (> length 0)
      (loop while (not (null elements))
	    do (setf key (pop elements)
		     value (pop elements))
	    (push (cons key value) alist))
      (nreverse alist))))

(defun item-profit (price quantity &optional fee)
  "Get item's profit based on the price, quantity, and a fee, usually of 10%
All arguments must be numbers. Fee is interpreted as a percentage value"
  (when (or (not fee) (not (numberp fee)))
    (setf fee 10))
  (let* ((offert (* price quantity))
	 (tax (/ (* offert fee) 100))
	 (profit (- offert tax)))
    (round profit)))

;; (with-eval-after-load "simple-httpd"
;;   (progn
;;     (defservlet* fim/:history text/html ( )
;;       (cd httpd-root)
;;       (setf history (or history ".*"))
;;       (let (file)
;; 	(setf file (directory-files-recursively "ff" history))
;; 	(if (> (length file) 1)
;; 	    (let* ((length (length file))
;; 		   (random (random (1- length))))
;; 	      (setf file (nth random file)))
;; 	  (setf file (car file)))
;; 	(insert-file-contents file)))

;;     (defservlet* space/:file text/html ( )
;;       (cd (expand-file-name "Read" httpd-root))
;;       (let (target)
;; 	(setf target (cond ((and (not (null file))
;;     				 (file-exists-p file))
;;     			    file)
;;     			   ((and (not (null file))
;; 				 (file-exists-p (expand-file-name file httpd-root)))
;; 			    (expand-file-name file httpd-root))
;;     			   (t
;;     			    "read.html")))
;; 	(insert-file-contents target)))))

(defun fix-vlc-list ( )
  (interactive)
  (let* ((match "<\\(location\\)>\\(.*\\)</\\1>") (replace "<\\(info\\)>\\(.*\\)</\\1>\n") text (count 0))
    (save-excursion
      (goto-char (point-min))
      (while (re-search-forward match nil t)
	(save-excursion
	  (save-match-data
	    (when (re-search-forward replace nil t)
	      (setf text (match-string-no-properties 2))
	      (replace-match ""))))
	(when text
	  (replace-match text t nil nil 2)
	  (setf count (1+ count)
		text nil))))
    (when (>= count 1)
      (message "%d ocurrences" count))
    count))

(defun int-to-binary-string ( integer )
  "Returns the binary string representation of INTEGER.
when argument is not an integer, point out the mistake and do nothing"
  (when (not (integerp integer))
    (user-error "`%s' is meant to be an integer" integer))
  
  (let ((bits ""))
    (while (not (= integer 0))
      (setf bits (concat (if (= 1 (logand integer 1)) "1" "0") bits)
	    integer (lsh integer -1)))
    (when (string= bits "")
      (setf bits "0"))
    bits))

(defun int-to-base ( number base )
  "Converts a positive decimal number NUMBER to its digit representation in base BASE

NUMBER must be an integer and BASE may be an integer between 2 and 36 or a string representing a custom set of symbols. 

In the latter case BASE will be equal to te length of that set, and the set itself will be used as the representation symbols, so order matters

Returns the string representation of NUMBER in the context of BASE"
  (let* ((symbols (if (or (integerp base)
			  (not (stringp base)))
		      "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
		    base))
	 (base (if (not (integerp base))
		   (length base)
		 base))
	 (digits "")
	 (digit 0)
	 (max-base (length symbols)))
    
    (cond ((< base 2)
	   (setf base 2))
	  ((> base max-base)
	   (setf base max-base)))
    
    (while (> number 0)
      (setf digit (mod number base)
	    digits (concat (substring symbols digit (1+ digit)) digits)
	    number (floor number base)))
    (when (string= digits "")
      (setf digits "0"))
    digits))

(defun base-to-int ( number base )
  "Converts a number representation NUMBER in base BASE into an integer base 10

NUMBER must be a string representing a number and BASE is the same as in `int-to-base'

Returns the integer representation of NUMBER"
  (let* ((symbols (if (or (integerp base)
			  (not (stringp base)))
		      "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
		    base))
	 (base (if (not (integerp base))
		   (length base)
		 base))
	 (integer 0)
	 (symbol ""))
    (let* ((domain (substring symbols 0 base))
	   (nan-base (concat "[^" domain "]")))
      (when (string-match-p nan-base number)
	(error "< %s > is not a number in the context of { %s }" number domain)))

    (setf number (split-string number "" t))
    (dolist (symbol number integer)
      (setf integer (+ (* integer base) (string-match symbol symbols))))))

(defun convert-base ( number x y )
  "Converts the representation of a number NUMBER base X into the equivalent representation in base Y"
  (when (not (and (or (integerp number)
		      (stringp number))
		  (or (integerp x)
		      (stringp x))
		  (or (integerp y)
		      (stringp y))))
    (user-error "convert-base: arguments may be either integers or strings"))
  
  (when (or (and (integerp x)
		 (or (< x 2)
		     (> x 36)))
	    (and (integerp y)
		 (or (< y 2)
		     (> y 36))))
    (user-error "convert-base: x and y arguments as integers may have a value between 2 and 36"))
  
  (let* ((symbols "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ")
	 (domain-x (if (stringp x)
		       x
		     (substring symbols 0 x)))
	 (domain-y (if (stringp y)
		       y
		     (substring symbols 0 y)))
	 (nan-x (concat "[^" domain-x "]"))
	 (nan-y (concat "[^" domain-y "]")))

    (when (string-match-p nan-x number)
      (error "< %s > is not a number in the context of { %s }" number domain-x))
    
    (cond ((equal x y)
	   number)
	  ((integerp number)
	   (unless (or (and (integerp y)
			    (= y 10))
		       (and (stringp y)
			    (string= y (substring symbols 0 10))))
	     (setf number (int-to-base number y))))
	  ((stringp number)
	   (if (and (integerp x)
		    (>= x 2)
		    (<= x 16))
	       (setf number (string-to-number number x))
	     (setf number (base-to-int number x)))
	   (unless (or (and (integerp y)
			    (= y 10))
		       (and (stringp y)
			    (string= y (substring symbols 0 10))))
	     (setf number (int-to-base number y)))))
    number))

(defun char-at-index ( string  index )
  "Returns char at position INDEX in string STRING"
  (char-to-string (elt string index)))

(defun index-of-char ( string char )
  "Returns position of character CHAR in string STRING as an integer"
  (let ((length (length string))
	(index 0)
	(found nil))
    (while (and (not found)
		(< index length))
      (when (equal char (char-at-index string index))
	(setf found t))
      (setf index (1+ index)))
    (when found
      index)))

(defmacro time-run (&rest form)
  "Measure the time it takes to evaluate FORM, in seconds."
  `(let ((time (current-time)))
     ,@form
     (format "%.06f" (float-time (time-since time)))))

(defun lgc ( )
  (mod (+ (* 0 #x6E81BDB831DD448D) 333666999) (expt 2 64)))
