;;; init.el --- Master Configuration File -*- lexical-binding: t -*-
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Basic features and recompilation on demand ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(eval-and-compile
  (require 'cl-lib) ;; I always use cl-lib
  (require 'bytecomp)) ;; Ensure the byte compiler is ready

(cl-eval-when (:compile-toplevel)
  (add-to-list 'load-path (expand-file-name "init.d"))
  (require 'compile-util))

(cl-block :init
  ;; Recompile this file if necesary
  (let* ((base (file-name-sans-extension (or load-file-name "")))
	 (init (concat base ".el"))
	 (byte (concat init "c")))
    (when (and load-file-name (file-newer-than-file-p init byte))
      (byte-recompile-file init nil 0)
      (load base t t)
      (cl-return-from :init)))
  
  ;; user-init-file is set to the eln file during init time
  ;; which may break later code relying on this variable at this time
  (let ((entry (expand-file-name "init.el" user-emacs-directory)))
    (unless (string= entry user-init-file)
      (setf user-init-file entry)))

  ;; load useful code early
  (add-to-list 'load-path (expand-file-name "init.d" user-emacs-directory))

  (require 'util)
  (require 'variables)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Custom ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  
  ;; Load Custom Config
  (setf custom-file (expand-file-name "custom-config.el" user-emacs-directory))
  (unless noninteractive (with-errors-as-warnings '(init) (format "(load %S): %%S" custom-file)
			   (load custom-file t t)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Modular Configuration ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  
  ;; Load Additional Config
  (when (file-directory-p user-init-directory)
    (let ((collection '(functions environment package-configuration)))
      
      (unless noninteractive
	(setf collection (append collection `(bindings frame ,(if (daemonp) 'daemon 'standalone) misc))))
      
      (cl-loop as entry in collection
	       as library = (expand-file-name (symbol-name entry) user-init-directory)
	       when (file-exists-p (concat library ".el"))
	       do (with-errors-as-warnings `(init ,entry) (format "(load %S): %%S" entry)
		    (load-maybe-recompile library))))))

(provide 'init)
